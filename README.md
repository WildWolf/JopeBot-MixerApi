# Mixer.NET
The community driven .NET Standard 2.0 implement for interacting with [Mixer](https://mixer.com)

[![Build status](https://ci.appveyor.com/api/projects/status/bs4l6lpvfufjfnpu?svg=true)](https://ci.appveyor.com/project/WildWolf/mixer-net)

You can grab the dll from [here](https://ci.appveyor.com/project/WildWolf/mixer-net/build/artifacts)

Dependencies
[JSON.NET](https://www.newtonsoft.com/json)
[MoreLINQ](https://github.com/morelinq/MoreLINQ)

Nested inside
[websocket-sharp](https://github.com/sta/websocket-sharp/)