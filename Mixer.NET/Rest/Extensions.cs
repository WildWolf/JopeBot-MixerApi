﻿using Mixer.NET.Models;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Mixer.NET.Rest
{
    public static class Extensions
    {
        public static async Task DownloadClip(this ClipProperties clip, string folder, Action<int> progress)
        {
            string fileName = $"{clip.ContentId}.mp4";
            Uri url = new Uri($"https://transcodedclips.blob.core.windows.net/clips/{fileName}");
            using (WebClient wc = new WebClient())
            {
                DateTime lastSignaled = DateTime.MinValue;
                wc.DownloadProgressChanged += (sender, args) =>
                {
                    DateTime now = DateTime.Now;
                    if (!(now - lastSignaled > TimeSpan.FromMilliseconds(500.0)))
                        return;
                    lastSignaled = now;
                    progress(args.ProgressPercentage);
                };
                while (true)
                {
                    try
                    {
                        await wc.DownloadFileTaskAsync(url, Path.Combine(folder, fileName));
                        progress(100);
                        break;
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Failed to download clip", ex);
                    }
                }
            }
        }

        public static async Task DownloadVod(this Recording recording, string folder, Action<int> progress)
        {
            if (recording.State == "AVAILABLE")
            {
                string url = recording.Vods.First().BaseUrl + "source.mp4";
                using (WebClient wc = new WebClient())
                {
                    DateTime lastSignaled = DateTime.MinValue;
                    wc.DownloadProgressChanged += (sender, args) =>
                    {
                        DateTime now = DateTime.Now;
                        if (!(now - lastSignaled > TimeSpan.FromMilliseconds(500.0)))
                            return;
                        lastSignaled = now;
                        progress(args.ProgressPercentage);
                    };
                    while (true)
                    {
                        try
                        {
                            await wc.DownloadFileTaskAsync(new Uri(url), Path.Combine(folder, recording.CreatedAt.ToString("yyyy'-'MM'-'dd'T'HH'-'mm'-'ss") + ".mp4"));
                            progress(100);
                            break;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Failed to download vod", ex);
                        }
                    }
                }
            }
            else
            {
                throw new Exception("Can't download unprocessed vods");
            }
        }
    }
}