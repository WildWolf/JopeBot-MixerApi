﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Mixer.NET.Models;
using Mixer.NET.Oauth;

using Newtonsoft.Json.Linq;

namespace Mixer.NET.Rest
{
    public partial class RestService
    {
        /// <summary>
        /// Initializes the short code auth flow in mixer
        /// </summary>
        /// <param name="clientId">The client id that is gotten from https://mixer.com/lab/oauth</param>
        /// <param name="scopes">The auth scopes this application requests access to</param>
        /// <param name="action">The callback action to handle new short code in case it times out and the tokens when a successful auth is done</param>
        /// <param name="clientSecret">The client secret that may be on the OAuth application if initialized with one</param>
        /// <returns>Check task for the short code auth flow</returns>
        public async Task<Task> ShortCodeAuth(string clientId, OAuthScopes scopes, Action<ShortCodeStruct> action, string clientSecret = null)
        {
            var task = await PostAsync($"{V1}/oauth/shortcode", new StringContent($@"{{""client_id"":""{clientId}"",{(clientSecret != null ? $@"""client_secret"":""{clientSecret}"",": "")}""scopes"":""{scopes.GetScopeString()}}}"));
            if (!task.IsSuccessStatusCode)
                throw new HttpRequestException("Mixer Shortcode Failed");

            var response = task.Content.ReadAsStringAsync().Result.Deserialize<OAuthShortCodeResponse>();
            var responseExpiration = DateTime.Now.AddSeconds(response.Expiration);
            action(new ShortCodeStruct(response.Code));
            return Task.Run(async () =>
            {
                var loop = true;
                while (loop)
                {
                    await Task.Delay(TimeSpan.FromSeconds(1));
                    if (responseExpiration > DateTime.Now)
                    {
                        task = await PostAsync($"{V1}/oauth/shortcode", new StringContent($@"{{""client_id"":""{clientId}"",{(clientSecret != null ? $@"""client_secret"":""{clientSecret}"",": "")}""scopes"":""{scopes.GetScopeString()}}}"));
                        if (!task.IsSuccessStatusCode)
                            throw new HttpRequestException("Mixer Shortcode Failed");

                        response = task.Content.ReadAsStringAsync().Result.Deserialize<OAuthShortCodeResponse>();
                        responseExpiration = DateTime.Now.AddSeconds(response.Expiration);
                        action(new ShortCodeStruct(response.Code, true));
                    }
                    var codeTask = await GetAsync($"{V1}/oauth/shortcode/check/{response.Handle}");
                    if (task.IsSuccessStatusCode)
                    {
                        if(task.StatusCode != HttpStatusCode.OK)
                            continue;

                        var code = task.Content.ReadAsStringAsync().Result.Deserialize<JObject>()["code"].ToString();
                        action(new ShortCodeStruct(null, true, await GetAuthToken(clientId, AuthGrantType.Code, code)));
                    }
                    else
                    {
                        if (task.StatusCode != HttpStatusCode.Forbidden)
                            continue;

                        loop = false;
                        action(new ShortCodeStruct(null, true));
                    }
                }
            });
        }

        /// <summary>
        /// Get the auth token depending on <paramref name="type"/> and <paramref name="typeString"/>
        /// </summary>
        /// <param name="clientId">The client id that is gotten from https://mixer.com/lab/oauth</param>
        /// <param name="type">The grant type to send</param>
        /// <param name="typeString">The code or refresh token to send in specified by <paramref name="type"/></param>
        /// <returns>The OAauthService holder for the access and refresh token</returns>
        public async Task<OAuthService> GetAuthToken(string clientId, AuthGrantType type, string typeString)
        {
            var response = await PostAsync($"{V1}/oauth/token", new StringContent("{" + $"\"refresh_token\":\"{typeString}\",\"client_id\":\"{clientId}\",\"grant_type\":\"{(type == AuthGrantType.Refresh ? "refresh_token" : "authorization_code")}\"" + "}", Encoding.UTF8, "application/json"));
            if(!response.IsSuccessStatusCode)
                throw new HttpRequestException($"Could not get info from {V1}/oauth/token");

            return response.Content.ReadAsStringAsync().Result.Deserialize<OAuthService>();
        }
    }

    public struct ShortCodeStruct
    {
        public OAuthService OAuthService { get; }

        public string Code { get; }

        public bool PreviousCodeExpired { get; }

        public ShortCodeStruct(string code, bool previousCodeExpired = false, OAuthService oAuth = null)
        {
            Code = code;
            PreviousCodeExpired = previousCodeExpired;
            OAuthService = oAuth;
        }

        public ShortCodeStruct(string code, bool previousCodeExpired) : this(code, previousCodeExpired, null)
        {
        }
    }

    public enum AuthGrantType
    {
        Refresh,
        Code
    }
}
