﻿using Mixer.NET.Chat;
using Mixer.NET.Models;
using Mixer.NET.Oauth;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Mixer.NET.Rest
{
    #region Class definitions
    public enum AuthHeader
    {
        Streamer,
        Bot
    }

    public enum ManifestType
    {
        FTL,
        M3U8,
        LIGHT2
    }
    #endregion

    public partial class RestService : HttpClient
    {
        public static readonly string ApiBaseAddress = "https://mixer.com/api";

        private Task authTask;

        private AuthenticationHeaderValue BotAuthHeader;

        private AuthenticationHeaderValue StreamerAuthHeader;

        public Action<AuthChanged> StreamerAuthChanged;
        public Action<AuthChanged> BotAuthChanged;

        public RestService()
        {
            this.BaseAddress = new Uri(ApiBaseAddress);
            this.DefaultRequestHeaders.Accept.Clear();
            this.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        internal string V1 => ApiBaseAddress + "/v1";

        internal string V2 => ApiBaseAddress + "/v2";

        public async Task SetAuthTokens(OAuthService streamerToken, OAuthService botToken = null, int delay = 21300)
        {
            var token = await GetAuthToken(streamerToken.ClientId, AuthGrantType.Refresh, streamerToken.RefreshToken);
            var ci = streamerToken.ClientId;
            streamerToken = token;
            token.ClientId = ci;

            if (botToken != null)
            {
                token = await GetAuthToken(botToken.ClientId, AuthGrantType.Refresh, botToken.RefreshToken);
                ci = botToken.ClientId;
                botToken = token;
                token.ClientId = ci;
            }

            SetAuth(streamerToken, botToken);

            authTask = Task.Delay(streamerToken.ExpiryDate-DateTime.UtcNow).ContinueWith(b => SetAuthTokens(streamerToken, botToken).GetAwaiter().GetResult(), TaskContinuationOptions.OnlyOnRanToCompletion);
        }

        public void SetAuth(OAuthService streamerAuthToken, OAuthService botAuthToken = null)
        {
            StreamerAuthChanged?.Invoke(streamerAuthToken.RefreshToken ?? "");
            if (botAuthToken != null) BotAuthChanged?.Invoke(botAuthToken.RefreshToken ?? "");
            if (botAuthToken == null) botAuthToken = streamerAuthToken;
            BotAuthHeader = new AuthenticationHeaderValue("Bearer", botAuthToken.AccessToken);
            StreamerAuthHeader = new AuthenticationHeaderValue("Bearer", streamerAuthToken.AccessToken);
            var _ = BotUsername;
            _ = StreamerUsername;
        }

        protected override void Dispose(bool disposing)
        {
            authTask.Dispose();
            base.Dispose(disposing);
        }

        #region Details Of Rest
        
        public ExpandedChannel BotChannel { get; private set; }

        public string BotUsername
        {
            get
            {
                if (BotChannel == null)
                {
                    var t = PostAsync($"{V1}/oauth/token/introspect", new StringContent($"{{\"token\":\"{BotAuthHeader.Parameter}\"}}", Encoding.UTF8, "application/json")).GetAwaiter().GetResult().Content.ReadAsStringAsync().GetAwaiter().GetResult();
                    botUsername = JsonConvert.DeserializeObject<JObject>(t)["username"].ToString();
                }
                return BotChannel.Token;
            }
        }

        public ExpandedChannel StreamerChannel { get; private set; }

        public string StreamerUsername
        {
            get
            {
                if (StreamerChannel == null)
                {
                    var t = PostAsync($"{V1}/oauth/token/introspect", new StringContent($"{{\"token\":\"{StreamerAuthHeader.Parameter}\"}}", Encoding.UTF8, "application/json")).GetAwaiter().GetResult().Content.ReadAsStringAsync().GetAwaiter().GetResult();
                    streamerUsername = JsonConvert.DeserializeObject<JObject>(t)["username"].ToString();
                }
                return StreamerChannel.Token;
            }
        }

        private string botUsername
        {
            set { BotChannel = GetChannel(value).GetAwaiter().GetResult(); }
        }

        private string streamerUsername
        {
            set { StreamerChannel = GetChannel(value).GetAwaiter().GetResult(); }
        }

        #endregion Details Of Rest

        private void ChangeAuthHeader(AuthHeader authHeader)
        {
            switch (authHeader)
            {
                case AuthHeader.Streamer:
                    DefaultRequestHeaders.Authorization = StreamerAuthHeader;
                    break;

                case AuthHeader.Bot:
                    DefaultRequestHeaders.Authorization = BotAuthHeader;
                    break;
            }
        }

        //Only place api calls that is not tiny

        #region Achivement Apis

        public async Task<List<Achivement>> GetAchivement()
        {
            return (await GetStringAsync($"{V1}/achievements")).Deserialize<List<Achivement>>();
        }

        #endregion Achivement Apis

        #region Broadcast Apis

        public async Task<Broadcast> GetBroadcastFromCurrent()
        {
            return (await GetStringAsync($"{V1}/broadcasts/current")).Deserialize<Broadcast>();
        }

        public async Task<Broadcast> GetBroadcastFromId(Guid id)
        {
            return (await GetStringAsync($"{V1}/broadcasts/{id.ToString()}")).Deserialize<Broadcast>();
        }

        /// <summary>
        /// Gets Manifest From Id
        /// </summary>
        /// <param name="id">Broadcast Id</param>
        /// <param name="type">Manifest Type</param>
        /// <returns>Stream of the manifest can either be file or string</returns>
        public async Task<Stream> GetBroadcastManifest(Guid id, ManifestType type)
        {
            switch (type)
            {
                case ManifestType.FTL:
                    return (await GetStreamAsync($"{V1}/broadcasts/{id.ToString()}/manifest.ftl"));

                case ManifestType.LIGHT2:
                    return (await GetStreamAsync($"{V1}/broadcasts/{id.ToString()}/manifest.light2"));

                case ManifestType.M3U8:
                    return (await GetStreamAsync($"{V1}/broadcasts/{id.ToString()}/manifest.m3u8"));
            }

            throw new ArgumentException("Invalid Manifest Type", nameof(type));
        }

        #endregion Broadcast Apis

        #region Chat Apis

        /// <summary>
        /// Gets the chat connection
        /// </summary>
        /// <param name="messageQueueTimeout">The timeout + 300 milliseconds + Random().Next(200)</param>
        /// <returns>A Chat Service witch contains all methods for sending and reciving messages and other events and methods for chat</returns>
        public async Task<ChatService> GetChat(TimeSpan messageQueueTimeout = default)
        {
            return await GetChat(StreamerChannel.Name);
        }

        /// <summary>
        /// Gets the chat connection
        /// </summary>
        /// <param name="tokenOrId">The Name or Id of the streamer to connect to</param>
        /// <param name="messageQueueTimeout">The timeout + 300 milliseconds + Random().Next(200)</param>
        /// <returns>A Chat Service witch contains all methods for sending and reciving messages and other events and methods for chat</returns>
        public async Task<ChatService> GetChat(string tokenOrId, TimeSpan messageQueueTimeout = default)
        {
            ChangeAuthHeader(AuthHeader.Bot);
            var k = await (
                await this.GetAsync(
                    $"{V1}/chats/{GetChannel(tokenOrId).Id}"
                )
            ).Content.ReadAsStringAsync();
            var t = JsonConvert.DeserializeObject<ChatConnection>(k);
            var f = new ChatService(t, this, messageQueueTimeout);
            return f;
        }

        public async Task<List<ChatUser>> GetChatUsers(int id)
        {
            return JsonConvert.DeserializeObject<List<ChatUser>>(
                await (
                    await this.GetAsync($"/chats/{id}/users")
                ).Content.ReadAsStringAsync()
            );
        }

        #endregion Chat Apis

        #region Clip Apis

        public async Task<List<ClipProperties>> GetClipsFromChannel(uint id)
        {
            return (await GetStringAsync($"{V1}/clips/channels/{id}")).Deserialize<List<ClipProperties>>();
        }

        #endregion Clip Apis

        #region Ingest Apis

        public async Task<List<Ingest>> GetIngests()
        {
            return JsonConvert.DeserializeObject<List<Ingest>>(await (await GetAsync(V1 + "/ingests")).Content.ReadAsStringAsync());
        }

        #endregion Ingest Apis

        #region User Apis

        public async Task<UserWithChannel> GetUserWithChannel(string v, bool getNew = false)
        {
            if (Cache.UserChannel.ContainsKey(v) && !getNew)
            {
                return Cache.UserChannel[v];
            }
            else
            {
                return Cache.UserChannel.Add(v, await GetUserWithChannelNew(v));
            }
        }

        private async Task<UserWithChannel> GetUserWithChannelNew(string v)
        {
            return (await GetStringAsync($"{V1}/users/{v}")).Deserialize<UserWithChannel>();
        }

        #endregion User Apis
    }

    public struct AuthChanged
    {
        public bool IsValid;
        public string Token;

        public static implicit operator AuthChanged(string s)
        {
            if (s == "")
                return false;
            return new AuthChanged{Token = s, IsValid = true};
        }

        public static implicit operator AuthChanged(bool b)
        {
            return new AuthChanged{Token = "Using Token auth not code auth", IsValid = false};
        }
    }
}