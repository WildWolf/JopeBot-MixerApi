﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Mixer.NET.Models;

namespace Mixer.NET.Rest
{
    public partial class RestService
    {
        
        public async Task<ExpandedChannel> GetChannel(uint id)
        {
            return await GetChannel(id.ToString());
        }

        public async Task<ExpandedChannel> GetChannel(string s)
        {
            var t = await GetStringAsync($"{V1}/channels/{s}");
            return (t).Deserialize<ExpandedChannel>();
        }

        public async Task<Analytic> GetChannelAnalytic(uint id, DateTime fromDate, DateTime toDate)
        {
            Dictionary<string, Task<string>> taskList = new Dictionary<string, Task<string>>
            {
                {"viewers", GetStringAsync($"{V1}/channels/{id}/analytics/tsdb/viewers")},
                {"viewersMetrics", GetStringAsync($"{V1}/channels/{id}/analytics/tsdb/viewersMetrics")},
                {"streamSessions", GetStringAsync($"{V1}/channels/{id}/analytics/tsdb/streamSessions")},
                {"streamHosts", GetStringAsync($"{V1}/channels/{id}/analytics/tsdb/streamHosts")},
                {"subscriptions", GetStringAsync($"{V1}/channels/{id}/analytics/tsdb/subscriptions")},
                {"followers", GetStringAsync($"{V1}/channels/{id}/analytics/tsdb/followers")},
                {"gameRanksGlobal", GetStringAsync($"{V1}/channels/{id}/analytics/tsdb/gameRanksGlobal")},
                {"subRevenue", GetStringAsync($"{V1}/channels/{id}/analytics/tsdb/subRevenue")},
                {"cpm", GetStringAsync($"{V1}/channels/{id}/analytics/tsdb/cpm")},
                {"viewerSessionCount", GetStringAsync($"{V1}/channels/{id}/analytics/tsdb/viewerSessionCount")}
            };

            while (!taskList.Select(t => t.Value).All(t => t.IsCompleted))
            {
                await Task.Delay(100);
            }

            return new Analytic
            {
                CpmAnalytic = taskList["cpm"].Result.Deserialize<CPMAnalytic>(),
                ViewerAnalytic = taskList["viewers"].Result.Deserialize<ViewerAnalytic>(),
                FollowersAnalytic = taskList["followers"].Result.Deserialize<FollowersAnalytic>(),
                GameRankAnalytic = taskList["gameRanksGlobal"].Result.Deserialize<GameRankAnalytic>(),
                StreamHostsAnalytic = taskList["streamHosts"].Result.Deserialize<StreamHostsAnalytic>(),
                StreamSessionsAnalytic = taskList["streamSessions"].Result.Deserialize<StreamSessionsAnalytic>(),
                SubRevenueAnalytic = taskList["subRevenue"].Result.Deserialize<SubRevenueAnalytic>(),
                SubscriptionsAnalytic = taskList["subscriptions"].Result.Deserialize<SubscriptionsAnalytic>(),
                ViewerMetricAnalytic = taskList["viewersMetrics"].Result.Deserialize<ViewerMetricAnalytic>(),
                ViewerSessionCountAnalytic = taskList["viewerSessionCount"].Result.Deserialize<ViewerSessionCountAnalytic>()
            };
        }

        public async Task<ExpandedChannel> GetChannelDetails(uint id)
        {
            return await GetChannelDetails(id.ToString());
        }

        public async Task<ExpandedChannel> GetChannelDetails(string s)
        {
            return (await GetStringAsync($"{V1}/channels/{s}/details")).Deserialize<ExpandedChannel>();
        }

        public async Task<List<Channel>> GetChannels(uint page = 0, uint limit = 50, string whereQuery = "", string additionalQuery = "")
        {
            return (await GetStringAsync($"{V1}/channels?page={page}&limit={limit}&where={whereQuery}&{additionalQuery}")).Deserialize<List<Channel>>();
        }

        public async Task<List<UserWithChannel>> GetFollowersForChannel(uint id, uint page = 0)
        {
            return (await GetStringAsync($"{V1}/channels/{id}/follow?page={page}")).Deserialize<List<UserWithChannel>>();
        }

        public async Task<uint> GetFolowersPageLenghtForChannel(uint id)
        {
            return Convert.ToUInt32((await GetAsync($"{V1}/channels/{id}/follow")).Headers.Where(t => t.Key == "Link").Select(t => t.Value.First()).Single().Split(',').Single(l => l.Contains("last")).Split('=', '>')[1]);
        }

        public async Task<List<Recording>> GetRecordingsForChannel(uint id, uint page = 0)
        {
            return (await GetStringAsync($"{V1}/channels/{id}/recordings?page={page}")).Deserialize<List<Recording>>();
        }

        public async Task<uint> GetRecordingsPageLengthForChannel(uint id)
        {
            return Convert.ToUInt32((await GetAsync($"{V1}/channels/{id}/recordings")).Headers.Where(t => t.Key == "Link").Select(t => t.Value.First()).Single().Split(',').Single(l => l.Contains("last")).Split('=', '>')[1]);
        }

        public async Task<Light2Manifest> GetLight2ManifestForChannel(uint id)
        {
            return (await GetStringAsync($"{V1}/channels/{id}/manifest.light2")).Deserialize<Light2Manifest>();
        }
    }
}
