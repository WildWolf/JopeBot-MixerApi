﻿using Mixer.NET.Models;
using Mixer.NET.Rest;

using MoreLinq.Extensions;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json.Converters;

namespace Mixer.NET.Chat
{
    public enum MetaTag
    {
        Whisper = 3,
        Me = 2,
        Censored = 1,
        None = 0
    }

    public static class MessageExtenstion
    {
        public static string GetFullText(this IEnumerable<MessageData> md)
        {
            return string.Join("", md.Select(d => d.Text));
        }

        public static MessageClass ProcessMessageString(this JObject inn, RestService rest)
        {
            MessageClass message = new MessageClass();
            message.Channel = inn["channel"].ToInt();
            message.Id = inn["id"].ToGuid();
            var msgA = (JArray)inn["message"]["message"];

            msgA.ForEach(msg =>
            {
                switch (msg["type"].ToString())
                {
                    case "text":
                        message.Messages.Add(msg.ToObject<TextMessageData>());
                        break;

                    case "emoticon":
                        message.Messages.Add(msg.ToObject<EmoticonMessageData>());
                        break;

                    case "link":
                        message.Messages.Add(msg.ToObject<LinkMessageData>());
                        break;

                    case "image":
                        message.Messages.Add(msg.ToObject<LinkMessageData>());
                        break;

                    case "tag":
                        message.Messages.Add(msg.ToObject<TagMessageData>());
                        break;
                }
            });

            message.Meta = inn["message"]["meta"].ToObject<JObject>().Properties().Select(d => d.Name).FirstOrDefault().getMetaFromString();
            message.User = rest.GetUserWithChannel(inn["user_id"].ToString()).GetAwaiter().GetResult();
            message.AscensionLevel = inn["user_ascension_level"].ToInt();
            message.Roles = (Role)inn["user_roles"].ToObject<List<Role>>().Select(t => (int)t).Sum();
            return message;
        }

        private static MetaTag getMetaFromString(this string inn)
        {
            switch (inn)
            {
                case "whisper":
                    return MetaTag.Whisper;

                case "me":
                    return MetaTag.Me;

                case "censored":
                    return MetaTag.Censored;

                default:
                    return MetaTag.None;
            }
        }
    }

    public class EmoticonCoordsData
    {
        [JsonProperty("height")]
        public int Height { get; set; }

        [JsonProperty("width")]
        public int Width { get; set; }

        [JsonProperty("x")]
        public int X { get; set; }

        [JsonProperty("y")]
        public int Y { get; set; }
    }

    public class EmoticonMessageData : MessageData
    {
        [JsonProperty("coords")]
        public EmoticonCoordsData Coords { get; set; }

        public override string OType => "EmoticonMessageData";

        [JsonProperty("pack")]
        public string Pack { get; set; }

        [JsonProperty("source")]
        public string Source { get; set; }
    }

    public class LinkMessageData : MessageData
    {
        public override string OType => "LinkMessageData";

        [JsonProperty("url")]
        public string Url { get; set; }
    }

    public class MessageClass : IJsonSerializable
    {
        public MessageClass()
        {
            Messages = new List<MessageData>();
        }

        [JsonProperty("channel")]
        public int Channel { get; set; }

        [JsonProperty("id")]
        public Guid Id { get; set; }
        
        public int AscensionLevel { get; set; }

        /// <summary>
        /// The full message text because fuck mixer chat objects with their stupid chat message splitting
        /// </summary>
        public string Message => Messages.GetFullText();

        public List<MessageData> Messages { get; }
        public MetaTag Meta { get; set; }
        public UserWithChannel User { get; set; }
        public Role Roles { get; set; }

        public override string ToString()
        {
            return this.Serialize();
        }
    }

    public abstract class MessageData
    {
        public abstract string OType { get; }

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public class TagMessageData : MessageData
    {
        [JsonProperty("id")]
        public uint Id { get; set; }

        public override string OType => "TagMessageData";

        [JsonProperty("username")]
        public string Username { get; set; }
    }

    public class TextMessageData : MessageData
    {
        [JsonProperty("data")]
        public string Data { get; set; }

        public override string OType => "TextMessageData";
    }
}