﻿using Newtonsoft.Json;

using System.Collections.Generic;

namespace Mixer.NET.Chat
{
    public class Poll
    {
        [JsonProperty("answers")]
        public string[] Answers { get; set; }

        [JsonProperty("author")]
        public UpdateUserUser Author { get; set; }

        [JsonProperty("duration")]
        public long Duration { get; set; }

        [JsonProperty("endsAt")]
        public ulong EndsAt { get; set; }

        [JsonProperty("originatingChannel")]
        public uint OriginatingChannel { get; set; }

        [JsonProperty("q")]
        public string Question { get; set; }

        [JsonProperty("responses")]
        public Dictionary<string, uint> Responses { get; set; }

        [JsonProperty("voters")]
        public uint Voters { get; set; }
    }
}