﻿using Mixer.NET.Models;

using Newtonsoft.Json;

namespace Mixer.NET.Chat
{
    public abstract class UpdateUser
    {
        public abstract string Type { get; }
    }

    public class UpdateUserUser
    {
        [JsonProperty("user_roles"), JsonConverter(typeof(RoleConverter))]
        public Role Role { get; set; }

        [JsonProperty("user_id")]
        public uint UserId { get; set; }

        [JsonProperty("user_name")]
        public string Username { get; set; }
    }

    public class UserJoin : UpdateUser
    {
        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("originatingChannel")]
        public uint OriginatingChannel { get; set; }

        [JsonProperty("roles"), JsonConverter(typeof(RoleConverter))]
        public Role Role { get; set; }

        public override string Type => "UserJoin";

        [JsonProperty("username")]
        public string Username { get; set; }
    }

    public class UserLeave : UpdateUser
    {
        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("originatingChannel")]
        public uint OriginatingChannel { get; set; }

        public override string Type => "UserLeave";

        [JsonProperty("username")]
        public string Username { get; set; }
    }

    public class UserTimeout : UpdateUser
    {
        [JsonProperty("duration")]
        public long Duration { get; set; }

        public override string Type => "UserTimeout";

        [JsonProperty("user")]
        public UpdateUserUser User { get; set; }
    }

    public class UserUpdate : UpdateUser
    {
        [JsonProperty("user")]
        public uint Id { get; set; }

        [JsonProperty("permissions")]
        public object[] Permissions { get; set; }

        [JsonProperty("roles"), JsonConverter(typeof(RoleConverter))]
        public Role Role { get; set; }

        public override string Type => "UserUpdate";

        [JsonProperty("username")]
        public string Username { get; set; }
    }
}