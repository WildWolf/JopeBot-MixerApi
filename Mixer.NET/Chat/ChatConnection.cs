﻿using Mixer.NET.Models;
using Newtonsoft.Json;
using System;

namespace Mixer.NET.Chat
{
    public class ChatConnection
    {
        [JsonProperty("authkey")]
        public string Authkey { get; set; }

        [JsonProperty("endpoints")]
        public string[] Endpoints { get; set; }

        [JsonProperty("permission")]
        public string[] Permissions { get; set; }

        public Role Roles { get; private set; }

        [JsonProperty("roles")]
        public string[] tmp1
        {
            set
            {
                string t = String.Join(", ", value);
                if (Enum.TryParse(t, out Role @enum))
                {
                    Roles = @enum;
                }
            }
        }
    }
}