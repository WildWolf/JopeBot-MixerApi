﻿using Mixer.NET.Rest;
using Newtonsoft.Json.Linq;
using WebSocketSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Mixer.NET.Chat
{
    public class ChatService : IDisposable
    {
        private CancellationTokenSource cancellationToken;
        private ChatConnection chatConnection;
        private WebSocket client;
        private MessageQueue messageQueue = new MessageQueue();
        private TimeSpan messageTimeout;
        private WebSocketState oldState;
        private RestService rest;

        public ChatService(ChatConnection chatConnection, RestService rest, TimeSpan messageTimeout)
        {
            client = new WebSocket(chatConnection.Endpoints.First());
            oldState = client.ReadyState;
            this.messageTimeout = messageTimeout;
            cancellationToken = new CancellationTokenSource();
            this.chatConnection = chatConnection;
            this.rest = rest;
            Welcome += ChatService_Welcome;
            RawChat += ChatService_RawChat;
            client.OnMessage += ClientOnOnMessage;
            Authenticated += b => { authenticating = false; }; 
            connect();
        }

        private void ClientOnOnMessage(object sender, MessageEventArgs e)
        {
            if (e.IsText)
            {
                RawChat(e.Data);
            }
        }

        public delegate void ChatMessage(MessageClass message);

        public delegate void CleanChat(UpdateUserUser user);

        public delegate void ConnectionEventEvent(WebSocketState state);

        public delegate void MessageHistory(List<MessageClass> messages);

        public delegate void PollEvent(Poll poll);

        public delegate void UpdateChatMessage(UpdateMessage message);

        public delegate void UserUpdateEvent(UpdateUser user);

        public event CleanChat ClearChat;

        public event ConnectionEventEvent ConnectionEvent;

        public event MessageHistory History;

        public event ChatMessage Message;

        public event PollEvent Poll;

        public Action<string> RawChat;

        public event UpdateChatMessage UpdateMessage;

        public event UserUpdateEvent UserUpdate;

        public Action Welcome;

        public Action<bool> Authenticated;

        public Action<Exception> Error;
        private bool authenticating = false;

        /// <summary>
        /// To add packets to queue if it already isn't pre made check https://dev.mixer.com/reference/chat/methods for methods
        /// </summary>
        /// <param name="method">The packet tag</param>
        /// <param name="arguments">Arguments for the packet</param>
        public void AddPacketToQueue(MethodTag method, List<object> arguments)
        {
            messageQueue.Add(new MessageQueueClass(arguments, method));
        }

        public void Dispose()
        {
            cancellationToken.Cancel();
            client.Dispose();
        }

        private async Task AddMessageQueueHandler()
        {
            while (true)
            {
                if (checkConnection())
                {
                    try
                    {
                        if (!authenticating)
                        {
                            var t = messageQueue.GetNextPacket();
                            if ((t.Key.Timeout - DateTime.UtcNow).TotalMilliseconds <= 0)
                            {
                                if (t.Key.Method == MethodTag.Auth) authenticating = true;
                                client.Send(t.Value);
                                t.Key.Timeout = DateTime.UtcNow + TimeSpan.FromSeconds(10);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                    await Task.Delay(messageTimeout.Milliseconds + new Random().Next(200));
                }
                else
                {
                    client.Connect();
                }
            }
        }

        private void Authenticate(uint channelId, uint userId, string authKey)
        {
            AddPacketToQueue(MethodTag.Auth, new List<object> { channelId, userId, authKey });
        }

        private void ChatService_RawChat(string s)
        {
            JObject jObject = JObject.Parse(s);
            string type = jObject["type"].ToString();

            if (type == "event")
            {
                switch (jObject["event"].ToString())
                {
                    case "WelcomeEvent":
                        Welcome?.Invoke();
                        break;

                    case "ChatMessage":
                        Message?.Invoke(((JObject)jObject["data"]).ProcessMessageString(rest));
                        break;

                    case "UserJoin":
                        UserUpdate?.Invoke(jObject["data"].ToObject<UserJoin>());
                        break;

                    case "UserLeave":
                        UserUpdate?.Invoke(jObject["data"].ToObject<UserLeave>());
                        break;

                    case "UserUpdate":
                        UserUpdate?.Invoke(jObject["data"].ToObject<UserUpdate>());
                        break;

                    case "UserTimeout":
                        UserUpdate?.Invoke(jObject["data"].ToObject<UserTimeout>());
                        break;

                    case "PollStart":
                        Poll?.Invoke(jObject["data"].ToObject<Poll>());
                        break;

                    case "PollEnd":
                        Poll?.Invoke(jObject["data"].ToObject<Poll>());
                        break;

                    case "DeleteMessage":
                        UpdateMessage?.Invoke(jObject["data"].ToObject<DeleteMessage>());
                        break;

                    case "PurgeMessage":
                        UpdateMessage?.Invoke(jObject["data"].ToObject<PurgeMessage>());
                        break;

                    case "ClearMessages":
                        ClearChat?.Invoke(jObject["data"]["clearer"].ToObject<UpdateUserUser>());
                        break;
                }
            }
            else
            {
                if (!jObject["error"].IsNullOrEmpty())
                {
                    Error?.Invoke(new Exception(jObject["error"].ToString()));
                    messageQueue.RemoveId(jObject["id"].ToInt());
                    return;
                }
                if (messageQueue[jObject["id"].ToInt()].Method == MethodTag.History)
                {
                    var t = new List<MessageClass>();

                    foreach (var jToken1 in (JArray)jObject["data"])
                    {
                        var jToken = (JObject)jToken1;

                        if (jToken != null)
                        {
                            t.Add(jToken.ProcessMessageString(rest));
                        }
                    }

                    History?.Invoke(t);
                }
                if (!jObject["data"].IsNullOrEmpty() && jObject["data"]["authenticated"] != null) Authenticated?.Invoke(jObject["data"]["authenticated"].ToObject<bool>());

                messageQueue.RemoveId(jObject["id"].ToInt());
            }
        }

        private void ChatService_Welcome()
        {
            Authenticate(rest.StreamerChannel.Id, rest.BotChannel.UserId, chatConnection.Authkey);
        }

        private bool checkConnection()
        {
            if (oldState != client.ReadyState)
            {
                oldState = client.ReadyState;
                ConnectionEvent?.Invoke(oldState);
            }
            return oldState == WebSocketState.Open || oldState == WebSocketState.Connecting;
        }

        private void connect()
        {
            client.Connect();
            var _ = Task.Run(async () =>
            {
                while (true)
                {
                    await Task.Delay(TimeSpan.FromSeconds(5));
                    var _ = client.IsAlive;
                }
            }, cancellationToken.Token);
            var __ = Task.Run(AddMessageQueueHandler, cancellationToken.Token);
        }

        #region Finished Chat Methods

        /// <summary>
        /// Cancels the skill event with the same id
        /// </summary>
        /// <param name="skillId">The skill id</param>
        public void CancelSkill(Guid skillId)
        {
            AddPacketToQueue(MethodTag.CancelSkill, new List<object> { skillId.ToString() });
        }

        /// <summary>
        /// Clears all messages in chat
        /// </summary>
        public void ClearMessages()
        {
            AddPacketToQueue(MethodTag.ClearMessages, new List<object>());
        }

        /// <summary>
        /// Deletes the message with the same id
        /// </summary>
        /// <param name="messageId">The message id</param>
        public void DeleteMessage(Guid messageId)
        {
            AddPacketToQueue(MethodTag.DeleteMessage, new List<object> { messageId.ToString() });
        }

        /// <summary>
        /// Removes all messages from the supplied username
        /// </summary>
        /// <param name="username">The username to purge</param>
        public void PurgeUser(string username)
        {
            AddPacketToQueue(MethodTag.Purge, new List<object> { username });
        }

        /// <summary>
        /// Sends message in chat
        /// </summary>
        /// <param name="message">The message to send</param>
        public void SendMessage(string message)
        {
            AddPacketToQueue(MethodTag.Message, new List<object> { message });
        }

        /// <summary>
        /// Sends a whisper to the supplied user
        /// </summary>
        /// <param name="message">The message to whisper</param>
        /// <param name="username">The user to whisper</param>
        public void SendWhisper(string message, string username)
        {
            AddPacketToQueue(MethodTag.Whisper, new List<object> { username, message });
        }

        /// <summary>
        /// Start a vote in chat
        /// </summary>
        /// <param name="question">The question to ask</param>
        /// <param name="duration">The duration of the question before counting the votes (Only seconds)</param>
        /// <param name="options">The options for users to choose</param>
        public void StartVote(string question, TimeSpan duration, params string[] options)
        {
            AddPacketToQueue(MethodTag.VoteStart, new List<object> { question, options, duration.TotalSeconds });
        }

        /// <summary>
        /// Timeouts the user with the set amount of delay
        /// </summary>
        /// <param name="username"></param>
        /// <param name="time"></param>
        public void TimeoutUser(string username, TimeSpan time)
        {
            AddPacketToQueue(MethodTag.Timeout, new List<object> { username, time.TotalSeconds });
        }

        #endregion Finished Chat Methods
    }
}