﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Mixer.NET.Oauth
{
    public class OAuthBuilder
    {
        private string clientId = "";
        private string redirectUri = "";
        private string state = "";
        private bool forcePromt = false;
        private bool token = false;
        private List<OAuthScopes> scopes = new List<OAuthScopes>();

        public string Build()
        {
            string escapedUriString = @"https://mixer.com/oauth/authorize?";
            string resType = token ? "token" : "code";
            escapedUriString += $"response_type={resType}";
            escapedUriString += $"&redirect_uri={Uri.EscapeDataString(redirectUri)}";
            escapedUriString += $"&client_id={clientId}";
            escapedUriString += $"&state={state}";
            escapedUriString += $"&scope={scopes.GetScopeEscapedString()}";
            if(forcePromt) escapedUriString += "&approval_prompt=force";
            return escapedUriString;
        }

        public OAuthBuilder SetClientId(string s)
        {
            clientId = s;
            return this;
        }

        public OAuthBuilder SetRedirectUri(string s)
        {
            redirectUri = s;
            return this;
        }

        public OAuthBuilder SetState(string s)
        {
            state = s;
            return this;
        }

        public OAuthBuilder SetForcePromt(bool b)
        {
            forcePromt = b;
            return this;
        }

        public OAuthBuilder AddScopes(OAuthScopes s)
        {
            scopes.Add(s);
            return this;
        }

        public OAuthBuilder ToggleResponseType()
        {
            token = !token;
            return this;
        }
    }

    public enum OAuthScopes : long
    {
        [Description("achievement:view:self")] achievement_view = 1L << 0,
        [Description("channel:analytics:self")] channel_analytics = 1L << 1,
        [Description("channel:clip:create:self")] channel_clip_create = 1L << 2,
        [Description("channel:clip:delete:self")] channel_clip_delete = 1L << 3,
        [Description("channel:costream:self")] channel_costream = 1L << 4,
        [Description("channel:deleteBanner:self")] channel_delete_banner = 1L << 5,
        [Description("channel:details:self")] channel_details = 1L << 6,
        [Description("channel:follow:self")] channel_follow = 1L << 7,
        [Description("channel:partnership")] channel_partnership = 1L << 8,
        [Description("channel:partnership:self")] channel_partnership_self = 1L << 9,
        [Description("channel:streamKey:self")] channel_stream_key = 1L << 10,
        [Description("channel:teststream:view:self")] channel_teststream_view = 1L << 11,
        [Description("channel:update:self")] channel_update = 1L << 12,
        [Description("chat:bypass_catbot")] chat_bypass_catbot = 1L << 13,
        [Description("chat:bypass_filter")] chat_bypass_filter = 1L << 14,
        [Description("chat:bypass_links")] chat_bypass_links = 1L << 15,
        [Description("chat:bypass_slowchat")] chat_bypass_slowchat = 1L << 16,
        [Description("chat:cancel_skill")] chat_cancel_skill = 1L << 17,
        [Description("chat:change_ban")] chat_change_ban = 1L << 18,
        [Description("chat:change_role")] chat_change_role = 1L << 19,
        [Description("chat:chat")] chat_chat = 1L << 20,
        [Description("chat:clear_messages")] chat_clear_messages = 1L << 21,
        [Description("chat:connect")] chat_connect = 1L << 22,
        [Description("chat:edit_options")] chat_edit_options = 1L << 23,
        [Description("chat:giveaway_start")] chat_giveaway_start = 1L << 24,
        [Description("chat:poll_start")] chat_poll_start = 1L << 25,
        [Description("chat:poll_vote")] chat_poll_vote = 1L << 26,
        [Description("chat:purge")] chat_purge = 1L << 27,
        [Description("chat:remove_message")] chat_remove_message = 1L << 28,
        [Description("chat:timeout")] chat_timeout = 1L << 29,
        [Description("chat:view_deleted")] chat_view_deleted = 1L << 30,
        [Description("chat:whisper")] chat_whisper = 1L << 31,
        [Description("delve:view:self")] delve_view = 1L << 32,
        [Description("interactive:manage:self")] interactive_manage = 1L << 33,
        [Description("interactive:robot:self")] interactive_robot = 1L << 34,
        [Description("invoice:view:self")] invoice_view = 1L << 35,
        [Description("log:view:self")] log_view = 1L << 36,
        [Description("oauth:manage:self")] oauth_manage = 1L << 37,
        [Description("recording:manage:self")] recording_manage = 1L << 38,
        [Description("redeemable:create:self")] redeemable_create = 1L << 39,
        [Description("redeemable:redeem:self")] redeemable_redeem = 1L << 40,
        [Description("redeemable:view:self")] redeemable_view = 1L << 41,
        [Description("resource:find:self")] resource_find = 1L << 42,
        [Description("subscription:cancel:self")] subscription_cancel = 1L << 43,
        [Description("subscription:create:self")] subscription_create = 1L << 44,
        [Description("subscription:renew:self")] subscription_renew = 1L << 45,
        [Description("subscription:view:self")] subscription_view = 1L << 46,
        [Description("team:administer")] team_administer = 1L << 47,
        [Description("team:manage:self")] team_manage = 1L << 48,
        [Description("transaction:cancel:self")] transaction_cancel = 1L << 49,
        [Description("transaction:view:self")] transaction_view = 1L << 50,
        [Description("user:analytics:self")] user_analytics = 1L << 51,
        [Description("user:details:self")] user_details = 1L << 52,
        [Description("user:getDiscordInvite:self")] user_get_discord_invite = 1L << 53,
        [Description("user:log:self")] user_log = 1L << 54,
        [Description("user:notification:self")] user_notification = 1L << 55,
        [Description("user:seen:self")] user_seen = 1L << 56,
        [Description("user:update:self")] user_update = 1L << 57,
        [Description("user:updatePassword:self")] user_update_password = 1L << 58,
        [Description("user:act_as")] user_act_as = 1L << 59,
        chat_bot = chat_bypass_catbot | chat_bypass_filter | chat_bypass_links | chat_bypass_slowchat | chat_cancel_skill | chat_change_ban | chat_change_role | chat_chat | chat_clear_messages | chat_connect | chat_edit_options | chat_giveaway_start | chat_poll_start | chat_poll_vote | chat_purge | chat_remove_message | chat_timeout | chat_view_deleted | chat_whisper | user_act_as,
        channel_all = channel_analytics | channel_clip_create | channel_clip_delete | channel_costream | channel_delete_banner | channel_details | channel_follow | channel_teststream_view | channel_update,
        user_all = user_analytics | user_details | user_get_discord_invite | user_notification | user_seen | resource_find | oauth_manage | invoice_view,
        redeemable_all = redeemable_create | redeemable_redeem | redeemable_view,
        all = chat_bot | channel_all | interactive_robot | user_all | redeemable_all | subscription_view
    }

    public static class OAuthExtensions
    {
        public static string GetScopeEscapedString(this IEnumerable<OAuthScopes> scopes)
        {
            return Uri.EscapeDataString(GetScopeString(scopes));
        }

        public static string GetScopeString(this IEnumerable<OAuthScopes> scopes)
        {
            return string.Join(" ",scopes.Select(t => t.GetScopeString()).ToList());
        }

        public static string GetScopeEscapedString(this OAuthScopes scopes)
        {
            return Uri.EscapeDataString(GetScopeString(scopes));
        }

        public static string GetScopeString(this OAuthScopes scopes)
        {
            var l = scopes.GetType().GetMember(scopes.GetType().GetEnumName(scopes) ?? throw new InvalidOperationException())[0].GetCustomAttributes(typeof(DescriptionAttribute), true).Cast<DescriptionAttribute>().Select(f => f.Description);
            var k = Enum.GetValues(scopes.GetType()).Cast<OAuthScopes>().Where(g => scopes.HasFlag(g) && scopes != g).SelectMany(g => g.GetType().GetMember(g.GetType().GetEnumName(g) ?? throw new InvalidOperationException()).SelectMany(j => j.GetCustomAttributes(typeof(DescriptionAttribute), false).Cast<DescriptionAttribute>().Select(f => f.Description))).ToList();
            k.AddRange(l);
            return string.Join(" ",k);
        }
    }
}
