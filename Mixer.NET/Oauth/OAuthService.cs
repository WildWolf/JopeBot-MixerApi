﻿using Newtonsoft.Json;
using System;

namespace Mixer.NET.Oauth
{
    public class OAuthService
    {
        public string AccessToken { get; set; }

        public string ClientId { get; set; }

        public DateTime ExpiryDate { get; set; }

        public string RefreshToken { get; set; }

        #region Json Convertstuffs

        [JsonProperty("access_token")]
        public string access
        {
            set
            {
                AccessToken = value;
            }
        }

        [JsonProperty("expires_in")]
        public string expires
        {
            set
            {
                ExpiryDate = DateTime.UtcNow + TimeSpan.FromSeconds(Convert.ToUInt32(value)).Subtract(TimeSpan.FromSeconds(2));
            }
        }

        [JsonProperty("refresh_token")]
        public string refresh
        {
            set
            {
                RefreshToken = value;
            }
        }

        #endregion Json Convertstuffs

        #region Constructors
        public OAuthService() { }

        /// <summary>
        /// Creates a new OAuthService with clientId and token
        /// </summary>
        /// <param name="token">The refresh or access token supplied from Mixer auth flow</param>
        /// <param name="clientId">The clientId supplied from Mixer oauth lab</param>
        /// <param name="tokenType">The TokenType for <paramref name="token"/></param>
        public OAuthService(string token, string clientId, TokenType tokenType = TokenType.Refresh)
        {
            switch (tokenType)
            {
                case TokenType.Refresh:
                    RefreshToken = token;
                    break;
                case TokenType.Access:
                    AccessToken = token;
                    break;
            }
            ClientId = clientId;
        }
        #endregion

        public string BuildRefreshString()
        {
            return "{" + $"\"refresh_token\":\"{RefreshToken}\",\"client_id\":\"{ClientId}\",\"grant_type\":\"refresh_token\"" + "}";
        }

        public enum TokenType
        {
            Refresh,
            Access
        }
    }
}