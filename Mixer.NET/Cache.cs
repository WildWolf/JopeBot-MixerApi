﻿using Mixer.NET.Models;
using System.Collections.Generic;

namespace Mixer.NET
{
    internal static class Cache
    {
        public static readonly RestDictionary<string, UserWithChannel> UserChannel = new RestDictionary<string, UserWithChannel>();

        public static void Depose()
        {
            UserChannel.Clear();
        }
    }

    public class RestDictionary<T1, T2> : Dictionary<T1, T2>
    {
        public new T2 Add(T1 key, T2 value)
        {
            base.Add(key, value);
            return value;
        }
    }
}