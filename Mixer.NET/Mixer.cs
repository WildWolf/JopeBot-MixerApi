﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using Mixer.NET.Rest;

namespace Mixer.NET
{
    public static class Mixer
    {
        /// <summary>
        /// Static rest service that can be used if not needing multiple oauth connections
        /// </summary>
        public static RestService RestService = new RestService();
    }
}
