﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mixer.NET
{
    public interface IJsonSerializable
    {

    }

    public static class Extensions
    {
        public static T Deserialize<T>(this string s)
        {
            return JsonConvert.DeserializeObject<T>(s, GetSettings());
        }

        public static string FirstCharToUpper(this string input)
        {
            switch (input)
            {
                case null: throw new ArgumentNullException(nameof(input));
                case "": throw new ArgumentException($"{nameof(input)} cannot be empty", nameof(input));
                default: return input.First().ToString().ToUpper() + input.Substring(1);
            }
        }

        public static string Serialize(this IJsonSerializable t)
        {
            return JsonConvert.SerializeObject(t, GetSettings());
        }

        public static Guid ToGuid(this JToken token)
        {
            return token.ToObject<Guid>();
        }

        public static int ToInt(this JToken token)
        {
            return (int)token;
        }

        public static uint ToUInt(this JToken token)
        {
            return (uint)token;
        }

        private static JsonSerializerSettings GetSettings()
        {
            return new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Ignore,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                Converters = new List<JsonConverter>
                { },
                DateFormatString = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'",
                DateTimeZoneHandling = DateTimeZoneHandling.Utc
            };
        }

        public static bool IsNullOrEmpty(this JToken token)
        {
            return (token == null) ||
                (token.Type == JTokenType.Array && !token.HasValues) ||
                (token.Type == JTokenType.Object && !token.HasValues) ||
                (token.Type == JTokenType.String && token.ToString() == String.Empty) ||
                (token.Type == JTokenType.Null);
        }
    }
}