﻿using System.Net.WebSockets;

namespace Mixer.NET.Interactive
{
    public enum InteractiveErrorCodes { }

    public class InteractiveService
    {
        private string[] endpoints;
        private ClientWebSocket ws = new ClientWebSocket();

        public InteractiveService()
        {
            ws.Options.SetRequestHeader("X-Protocol-Version", "2.0");
        }

        public void Setup(string oAuthToken, uint interactiveVersion, string[] endpoints)
        {
            ws.Options.SetRequestHeader("Authorization", oAuthToken);
            ws.Options.SetRequestHeader("X-Interactive-Version", interactiveVersion.ToString());
            this.endpoints = endpoints;
        }
    }
}