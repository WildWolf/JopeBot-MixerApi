﻿using Mixer.NET.Models;

using Newtonsoft.Json;

namespace Mixer.NET.Constellation.Models
{
    public class Hostee
    {
        [JsonProperty("hoster")]
        public Channel Hoster { get; set; }

        [JsonProperty("hosterId")]
        public uint HosterId { get; set; }

        public string Type { get; set; }
    }
}