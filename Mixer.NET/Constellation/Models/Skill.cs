﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using System;

namespace Mixer.NET.Constellation.Models
{
    public class Skill
    {
        [JsonProperty("channelId")]
        public uint ChannelId { get; set; }

        [JsonProperty("executionId")]
        public Guid ExecutionId { get; set; }

        [JsonProperty("manifest")]
        public JObject Manifest { get; set; }

        [JsonProperty("parameter")]
        public JObject Parameter { get; set; }

        [JsonProperty("skillId")]
        public Guid SkillId { get; set; }

        [JsonProperty("socketUrl")]
        public string SocketUrl { get; set; }

        [JsonProperty("triggeringUserId")]
        public uint TriggeringUserId { get; set; }
    }
}