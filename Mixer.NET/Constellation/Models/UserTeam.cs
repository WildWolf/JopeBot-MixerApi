﻿using Mixer.NET.Models;
using Newtonsoft.Json;

namespace Mixer.NET.Constellation.Models
{
    public class UserTeam
    {
        [JsonProperty("team")]
        public Team Team { get; set; }

        public string Type { get; set; }
    }
}