﻿using Mixer.NET.Models;
using Newtonsoft.Json;

namespace Mixer.NET.Constellation.Models
{
    public class MemberUpdate
    {
        public string Type { get; set; }

        [JsonProperty("user")]
        public User User { get; set; }
    }
}