﻿using Newtonsoft.Json;
using System;

namespace Mixer.NET.Constellation.Models
{
    public class Patronage
    {
        [JsonProperty("currentMilestoneGroupId")]
        public uint CurrentMilestoneGroupId { get; set; }

        [JsonProperty("currentMilestoneId")]
        public uint CurrentMilestoneId { get; set; }

        [JsonProperty("patronageEarned")]
        public uint PatronageEarned { get; set; }

        [JsonProperty("patronagePeriodId")]
        public Guid PatronagePeriodId { get; set; }
    }
}