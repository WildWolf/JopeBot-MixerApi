﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Mixer.NET.Constellation
{
    [Flags]
    public enum MethodTag : long
    {
        [Description("announcement:announce")]
        Announcement = 1L << 0,

        [Description("channel:{id}:followed")]
        ChannelFollowed = 1L << 1,

        [Description("channel:{id}:hosted")]
        ChannelHosted = 1L << 2,

        [Description("channel:{id}:unhosted")]
        ChannelUnhosted = 1L << 3,

        [Description("channel:{id}:subscribed")]
        ChannelSubscribed = 1L << 4,

        [Description("channel:{id}:resubscribed")]
        ChannelResubscribed = 1L << 5,

        [Description("channel:{id}:resubShared")]
        ChannelResubShared = 1L << 6,

        [Description("channel:{id}:subscriptionGifted")]
        ChannelGiftSub = 1L << 7,

        [Description("channel:{id}:directPurchased")]
        ChannelDirectPurchased = 1L << 8,

        [Description("channel:{id}:update")]
        ChannelUpdate = 1L << 9,

        [Description("costream:{id}:update")]
        CostreamUpdate = 1L << 10,

        [Description("interactive:{id}:connect")]
        InteractiveConnect = 1L << 11,

        [Description("interactive:{id}:disconnect")]
        InteractiveDisconnect = 1L << 12,

        [Description("team:{id}:deleted")]
        TeamDeleted = 1L << 13,

        [Description("team:{id}:memberAccepted")]
        TeamMemberAccepted = 1L << 14,

        [Description("team:{id}:memberInvited")]
        teamMemberInvited = 1L << 15,

        [Description("team:{id}:memberRemoved")]
        TeamMemberRemoved = 1L << 16,

        [Description("team:{id}:ownerChanged")]
        TeamOwnerChanged = 1L << 17,

        [Description("user:{id}:achievement")]
        UserAchievement = 1L << 18,

        [Description("user:{id}:followed")]
        UserFollowed = 1L << 19,

        [Description("user:{id}:notify")]
        UserNotify = 1L << 20,

        [Description("user:{id}:subscribed")]
        UserSubscribed = 1L << 21,

        [Description("user:{id}:resubscribed")]
        UserResubscribed = 1L << 22,

        [Description("user:{id}:subscriptionGifted")]
        UserGiftSub = 1L << 23,

        [Description("user:{id}:teamAccepted")]
        UserTeamAccepted = 1L << 24,

        [Description("user:{id}:teamInvited")]
        UserTeamInvited = 1L << 25,

        [Description("user:{id}:teamRemoved")]
        UserTeamRemoved = 1L << 26,

        [Description("user:{id}:update")]
        UserUpdate = 1L << 27,

        [Description("channel:{id}:skill")]
        ChannelSkill = 1L << 28,

        [Description("channel:{id}:patronageUpdate")]
        ChannelPatronageUpdate = 1L << 29,

        [Description("livesubscribe")]
        Subscribe = 1L << 30,

        [Description("liveunsubscribe")]
        Unsubscribe = 1L << 31,

        [Description("ping")]
        Ping = 1L << 32,
    }

    internal static class MessageQueueExtension
    {
        public static IEnumerable<Enum> GetFlags(this Enum input)
        {
            foreach (Enum value in Enum.GetValues(input.GetType()))
                if (input.HasFlag(value))
                    yield return value;
        }

        public static string GetPacket(this MessageQueueClass message, int counter)
        {
            JObject ret = new JObject();
            JObject parms = new JObject();
            List<string> events = new List<string>();
            string method = "";
            MethodTag eventTag = message.Method;
            if (eventTag.HasFlag(MethodTag.Subscribe))
            {
                method = "livesubscribe";
                eventTag = eventTag ^ MethodTag.Subscribe;
            }
            if (eventTag.HasFlag(MethodTag.Unsubscribe))
            {
                method = "liveunsubscribe";
                eventTag = eventTag ^ MethodTag.Unsubscribe;
            }
            if (eventTag.HasFlag(MethodTag.Ping))
            {
                method = "ping";
                eventTag = eventTag ^ MethodTag.Ping;
            }
            foreach (MethodTag flag in eventTag.GetFlags())
            {
                var _f = flag.GetType().GetMember(flag.GetType().GetEnumName(flag))[0].GetCustomAttributes(typeof(DescriptionAttribute), false).First() as DescriptionAttribute;
                string ev = _f.Description;
                ev = ev.Replace("{id}", message.Arg.ToString());
                events.Add(ev);
            }
            parms.Add("events", JArray.FromObject(events));
            ret.Add("type", "method");
            ret.Add("method", method);
            ret.Add("params", parms);
            if (message.LastId == 0)
                message.LastId = counter++;
            ret.Add("id", message.LastId);
            return ret.ToString(Formatting.None);
        }
    }

    internal class MessageQueue
    {
        private int _counter;
        private List<MessageQueueClass> messages = new List<MessageQueueClass>();
        public MessageQueueClass this[int index] => messages[index];

        public void Add(MessageQueueClass message)
        {
            message.LastId = _counter++;
            messages.Add(message);
        }

        public MessageQueueClass GetNext()
        {
            var _message = messages.FirstOrDefault();
            messages.Remove(_message);
            messages.Add(_message);
            return _message;
        }

        public KeyValuePair<MessageQueueClass, string> GetNextPacket()
        {
            var message = GetNext();
            return new KeyValuePair<MessageQueueClass, string>(message, message.GetPacket(_counter));
        }

        public void Remove(MessageQueueClass message)
        {
            messages.Remove(message);
        }

        public void RemoveId(int id)
        {
            messages.Remove(messages.FirstOrDefault(t => t.LastId == id));
        }

        public bool HasPackets => messages.Any();
    }

    internal class MessageQueueClass
    {
        public MessageQueueClass(uint arg, MethodTag method)
        {
            Arg = arg;
            Method = method;
        }

        public uint Arg { get; set; }
        public int LastId { get; set; }
        public MethodTag Method { get; }

        private string Packet 
        {
            get
            {
                JObject ret = new JObject();
                JObject parms = new JObject();
                List<string> events = new List<string>();
                string method = "";
                MethodTag eventTag = Method;
                if (eventTag.HasFlag(MethodTag.Subscribe))
                {
                    method = "livesubscribe";
                    eventTag = eventTag ^ MethodTag.Subscribe;
                }

                if (eventTag.HasFlag(MethodTag.Unsubscribe))
                {
                    method = "liveunsubscribe";
                    eventTag = eventTag ^ MethodTag.Unsubscribe;
                }

                if (eventTag.HasFlag(MethodTag.Ping))
                {
                    method = "ping";
                    eventTag = eventTag ^ MethodTag.Ping;
                }

                foreach (MethodTag flag in eventTag.GetFlags())
                {
                    var _f = flag.GetType().GetMember(flag.GetType().GetEnumName(flag))[0]
                        .GetCustomAttributes(typeof(DescriptionAttribute), false).First() as DescriptionAttribute;
                    string ev = _f.Description;
                    ev = ev.Replace("{id}", Arg.ToString());
                    events.Add(ev);
                }

                parms.Add("events", JArray.FromObject(events));
                ret.Add("type", "method");
                ret.Add("method", method);
                ret.Add("params", parms);
                ret.Add("id", LastId);
                return ret.ToString(Formatting.None);
            }
        }
    }
}