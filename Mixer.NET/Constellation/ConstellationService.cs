﻿using Mixer.NET.Constellation.Models;
using Mixer.NET.Models;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using WebSocketSharp;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Mixer.NET.Constellation
{
    public static class Extensions
    {
        public static T AddType<T>(this JToken token, string type)
        {
            var r = (JObject)token;
            r.Merge(JObject.Parse($"{{\"Type\"=\"{type}\"}}"));

            return r.ToObject<T>();
        }
    }

    public class ConstellationService : IDisposable
    {
        private CancellationTokenSource cancellationToken = new CancellationTokenSource();
        private WebSocket client;
        private MessageQueue messageQueue = new MessageQueue();
        private WebSocketState oldState;

        public ConstellationService()
        {
            client = new WebSocket("wss://constellation.mixer.com");
            var _ = Task.Run(async () =>
            {
                while (true)
                {
                    await Task.Delay(TimeSpan.FromSeconds(10));
                    var _ = client.IsAlive;
                }
            }, cancellationToken.Token);
            RawResponse += OnRawResponse;
            client.OnMessage += ClientOnOnMessage;
        }

        private void ClientOnOnMessage(object sender, MessageEventArgs e)
        {
            if (e.IsText)
            {
                RawResponse(e.Data);
            }
        }

        #region Channel Events

        public delegate void ChannelFollowedUpdate(CFUpdate update, uint id);

        public delegate void CUUpdate(JObject jObject, uint id);

        public delegate void DirectPurchase(string s, uint id);

        public delegate void HosteeUpdate(Hostee hostee, uint id);

        public delegate void PatronageEvent(Patronage patronage, uint id);

        public delegate void SkillEvent(Skill skill, uint id);

        public delegate void SubEvent(CSubscribed cSubscribed, uint id);

        public event ChannelFollowedUpdate ChannelFollowed;

        public event CUUpdate ChannelUpdate;

        public event HosteeUpdate HostUpdate;

        public event PatronageEvent Patronage;

        public event DirectPurchase Purchase;

        public event SkillEvent Skill;

        public event SubEvent Subscription;

        #endregion Channel Events

        #region Div events

        public delegate void AnnouncementEvent(Announcement announcement);

        public delegate void ConnectionEventEvent(WebSocketState state);

        public delegate void CostreamEvent(Costream costream, uint id);

        public event AnnouncementEvent Announcement;

        public event ConnectionEventEvent ConnectionEvent;

        public event CostreamEvent Costream;

        #endregion Div events

        #region Interactive Events

        public delegate void InteractiveConnectionEvent(string server, uint id);

        public event InteractiveConnectionEvent InteractiveConnection;

        #endregion Interactive Events

        #region Team Events

        public delegate void TeamDeletedEvent(Team team, uint id);

        public delegate void TeamMemberUpdateEvent(MemberUpdate member, uint id);

        public delegate void TeamOwnershipChangeEvent(User user, uint id);

        public event TeamDeletedEvent TeamDeleted;

        public event TeamMemberUpdateEvent TeamMemberUpdate;

        public event TeamOwnershipChangeEvent TeamOwnershipChanged;

        #endregion Team Events

        #region User Events

        public delegate void UserAchievementEvent(AchivementEarning achivement, uint id);

        public delegate void UserFollowUpdateEvent(UFUpdate update, uint id);

        public delegate void UserNotifyEvent(Notification notification, uint id);

        public delegate void UserSubscriptionUpdateEvent(USubscribed subscribed, uint id);

        public delegate void UserTeamUpdateEvent(UserTeam team, uint id);

        public event UserAchievementEvent UserAchievement;

        public event UserFollowUpdateEvent UserFollowUpdate;

        public event UserNotifyEvent UserNotification;

        public event UserSubscriptionUpdateEvent UserSubscriptionUpdate;

        public event UserTeamUpdateEvent UserTeamUpdate;

        public event CUUpdate UserUpdate;

        #endregion User Events

        public Action<string> RawResponse;

        public void AddPacket(MethodTag method, uint id)
        {
            messageQueue.Add(new MessageQueueClass(id, method));
        }

        public void Connect(string clientId, bool isBot = true)
        {
            client.Connect();
            var _ = Task.Run(addMessageHandler, cancellationToken.Token);
        }

        public void Dispose()
        {
            cancellationToken.Cancel();
            cancellationToken.Dispose();
            client?.Dispose();
        }

        private async Task addMessageHandler()
        {
            while (true)
            {
                if (CheckConnection())
                {
                    if (messageQueue.HasPackets)
                    {
                        var message = messageQueue.GetNextPacket();
                        client.Send(message.Value);
                    }
                }
                else
                {
                    client.Connect();
                }
                await Task.Delay(200 + new Random().Next(200));
            }
        }

        private bool CheckConnection()
        {
            if (oldState != client.ReadyState)
            {
                oldState = client.ReadyState;
                ConnectionEvent?.Invoke(oldState);
            }
            return oldState == WebSocketState.Open || oldState == WebSocketState.Connecting;
        }

        private void OnRawResponse(string s)
        {
            JObject jObject = JObject.Parse(s);

            if (jObject["type"].ToString() == "event" && jObject["event"].ToString() == "live")
            {
                JObject data = (JObject)jObject["data"];
                string[] type = data["channel"].ToString().Split(':');
                uint id = 0;

                JToken payload = data["payload"];

                if (type.Length >= 2)
                {
                    id = Convert.ToUInt32(type[1]);
                }

                switch (type[0])
                {
                    case "announcement":
                        Announcement?.Invoke(payload.ToObject<Announcement>());
                        break;

                    case "channel":
                        switch (type[2])
                        {
                            case "followed":
                                ChannelFollowed?.Invoke(payload.ToObject<CFUpdate>(), id);
                                break;

                            case "hosted":
                                HostUpdate?.Invoke(payload.AddType<Hostee>("Hosted"), id);
                                break;

                            case "unhosted":
                                HostUpdate?.Invoke(payload.AddType<Hostee>("Unhosted"), id);
                                break;

                            case "subscribed":
                                Subscription?.Invoke(payload.AddType<CSubscribed>("Subscribed"), id);
                                break;

                            case "resubscribed":
                                Subscription?.Invoke(payload.AddType<CResubscribed>("Resubscribed"), id);
                                break;

                            case "resubShared":
                                Subscription?.Invoke(payload.AddType<CResubscribed>("ResubShared"), id);
                                break;

                            case "subscriptionGifted":
                                Subscription?.Invoke(payload.AddType<CSubscriptionGifted>("SubscriptionGifted"), id);
                                break;

                            case "directPurchase":
                                Purchase?.Invoke(payload["gameTitle"].ToObject<string>(), id);
                                break;

                            case "update":
                                ChannelUpdate?.Invoke((JObject)payload, id);
                                break;

                            case "skill":
                                Skill?.Invoke(payload.ToObject<Skill>(), id);
                                break;

                            case "patronageUpdate":
                                Patronage?.Invoke(payload.ToObject<Patronage>(), id);
                                break;
                        }
                        break;

                    case "costream":
                        Costream?.Invoke(payload.ToObject<Costream>(), id);
                        break;

                    case "interactive":
                        InteractiveConnection?.Invoke(payload.ToString(), id);
                        break;

                    case "team":
                        switch (type[2])
                        {
                            case "deleted":
                                TeamDeleted?.Invoke(payload.ToObject<Team>(), id);
                                break;

                            case "memberAccepted":
                                TeamMemberUpdate?.Invoke(payload.AddType<MemberUpdate>("MemberAccepted"), id);
                                break;

                            case "memberInvited":
                                TeamMemberUpdate?.Invoke(payload.AddType<MemberUpdate>("MemberInvited"), id);
                                break;

                            case "memberRemoved":
                                TeamMemberUpdate?.Invoke(payload.AddType<MemberUpdate>("MemberRemoved"), id);
                                break;

                            case "ownerChanged":
                                TeamOwnershipChanged?.Invoke(payload.ToObject<User>(), id);
                                break;
                        }
                        break;

                    case "user":
                        switch (type[2])
                        {
                            case "achievement":
                                UserAchievement?.Invoke(payload.ToObject<AchivementEarning>(), id);
                                break;

                            case "followed":
                                UserFollowUpdate?.Invoke(payload.ToObject<UFUpdate>(), id);
                                break;

                            case "notify":
                                UserNotification?.Invoke(payload.ToObject<Notification>(), id);
                                break;

                            case "subscribed":
                                UserSubscriptionUpdate?.Invoke(payload.AddType<USubscribed>("Subscribed"), id);
                                break;

                            case "resubscribed":
                                UserSubscriptionUpdate?.Invoke(payload.AddType<UResubscribed>("Resubscribed"), id);
                                break;
                            
                            case "subscriptionGifted":
                                UserSubscriptionUpdate?.Invoke(payload.AddType<USubscriptionGifted>("SubscriptionGifted"), id);
                                break;

                            case "teamAccepted":
                                UserTeamUpdate?.Invoke(payload.AddType<UserTeam>("TeamAccepted"), id);
                                break;

                            case "teamInvited":
                                UserTeamUpdate?.Invoke(payload.AddType<UserTeam>("TeamInvited"), id);
                                break;

                            case "teamRemoved":
                                UserTeamUpdate?.Invoke(payload.AddType<UserTeam>("TeamRemoved"), id);
                                break;

                            case "update":
                                UserUpdate?.Invoke((JObject)payload, id);
                                break;
                        }
                        break;
                }
            }
            else if (jObject["type"].ToString() == "reply")
            {
                messageQueue.RemoveId(jObject["id"].ToInt());
            }
        }
    }
}