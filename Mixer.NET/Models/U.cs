﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Mixer.NET.Models
{
    public class User : TimeStamped
    {
        [JsonProperty("avatarUrl")]
        public string AvatarUrl { get; set; }

        [JsonProperty("experience")]
        public uint Experience { get; set; }

        [JsonProperty("groups")]
        public List<UserGroup> Groups { get; set; }

        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("level")]
        public uint Level { get; set; }

        [JsonProperty("primaryTeam")]
        public uint PrimaryTeam { get; set; }

        [JsonProperty("social")]
        public SocialInfo Social { get; set; }

        [JsonProperty("sparks")]
        public uint Sparks { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("verified")]
        public bool Verified { get; set; }
    }

    public class UserGroup : TimeStamped
    {
        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("role")]
        public Role Role { get; set; }
    }

    public class UserInteractiveMetric : IJsonSerializable
    {
        [JsonProperty("browser")]
        public string Browser { get; set; }

        [JsonProperty("channelId")]
        public string ChannelId { get; set; }

        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("inputsGiven")]
        public uint InputsGiven { get; set; }

        [JsonProperty("platform")]
        public string Platform { get; set; }

        [JsonProperty("playtime")]
        public uint Playtime { get; set; }

        [JsonProperty("referrer")]
        public string Referrer { get; set; }

        [JsonProperty("region")]
        public string Region { get; set; }

        [JsonProperty("sparksSpent")]
        public uint SparksSpent { get; set; }

        [JsonProperty("timestamp")]
        public DateTime Timestamp { get; set; }

        [JsonProperty("timeToInteract")]
        public uint TimeToInteract { get; set; }

        [JsonProperty("userId")]
        public string UserId { get; set; }

        [JsonProperty("versionId")]
        public string VersionId { get; set; }
    }

    public class UserLog : IJsonSerializable
    {
        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("event")]
        public string Event { get; set; }

        [JsonProperty("eventData")]
        public object EventData { get; set; }

        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("source")]
        public string Source { get; set; }

        [JsonProperty("sourceData")]
        public object SourceData { get; set; }

        [JsonProperty("userId")]
        public uint UserId { get; set; }
    }

    public class UserPreferences : IJsonSerializable
    {
        [JsonProperty("channel:mature:allowed")]
        public bool? ChannelMatureAllowed { get; set; }

        [JsonProperty("channel:notifications")]
        public UserPreferencesNotifications ChannelNotifications { get; set; }

        [JsonProperty("channel:player:forceflash")]
        public bool? ChannelPlayerForceFlash { get; set; }

        [JsonProperty("chat:chromakey")]
        public bool? ChatChromakey { get; set; }

        [JsonProperty("chat:colors")]
        public bool? ChatColors { get; set; }

        [JsonProperty("chat:lurkmode")]
        public bool? ChatLurkmode { get; set; }

        [JsonProperty("chat:sounds:html5")]
        public bool? ChatSoundsHtml5 { get; set; }

        [JsonProperty("chat:sounds:play")]
        public bool? ChatSoundsPlay { get; set; }

        [JsonProperty("chat:sounds:volume")]
        public decimal ChatSoundsVolume { get; set; }

        [JsonProperty("chat:tagging")]
        public bool? ChatTagging { get; set; }

        [JsonProperty("chat:timestamps")]
        public bool? ChatTimestamps { get; set; }

        [JsonProperty("chat:whispers")]
        public bool? ChatWhipsers { get; set; }
    }

    public class UserPreferencesNotifications : IJsonSerializable
    {
        [JsonProperty("ids")]
        public List<string> Ids { get; set; }

        [JsonProperty("transports")]
        public List<string> Transports { get; set; }
    }

    public class UserWithChannel : User
    {
        [JsonProperty("channel")]
        public Channel Channel { get; set; }
    }
}