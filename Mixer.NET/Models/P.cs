﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Mixer.NET.Models
{
    public class PartnershipApp : TimeStamped
    {
        [JsonProperty("reapplies")]
        public DateTime? Reapplies { get; set; }

        [JsonProperty("reason")]
        public string Reason { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }

    public class PartnershipCriterion : TimeStamped
    {
        [JsonProperty("comparison")]
        public string Comparison { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("id")]
        public decimal Id { get; set; }

        [JsonProperty("isEnabled")]
        public bool IsEnabled { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("target")]
        public decimal Target { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public class PartnershipCriterionResult : IJsonSerializable
    {
        [JsonProperty("actual")]
        public decimal Actual { get; set; }

        [JsonProperty("comparison")]
        public string Comparison { get; set; }

        [JsonProperty("criterionId")]
        public string CriterionId { get; set; }

        [JsonProperty("succeeded")]
        public bool Succeeded { get; set; }

        [JsonProperty("target")]
        public decimal Target { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public class PartnershipEvaluationResult : IJsonSerializable
    {
        [JsonProperty("accountAge")]
        public PartnershipCriterionResult AccountAge { get; set; }

        [JsonProperty("daysStreamedLastMonth")]
        public PartnershipCriterionResult DaysStreamedLastMonth { get; set; }

        [JsonProperty("followers")]
        public PartnershipCriterionResult Followers { get; set; }

        [JsonProperty("hoursStreamedLastMonth")]
        public PartnershipCriterionResult HoursStreamedLastMonth { get; set; }

        [JsonProperty("succeeded")]
        public bool Succeeded { get; set; }

        [JsonProperty("totalViewers")]
        public PartnershipCriterionResult TotalViewers { get; set; }
    }

    public class PartyChatCreate : IJsonSerializable
    {
        [JsonProperty("deviceUUID")]
        public string DeviceUUID { get; set; }

        [JsonProperty("protocolVersion")]
        public decimal ProtocolVersion { get; set; }

        [JsonProperty("webRtcDtlsCertificateAlgorithm")]
        public string WebRtcDtlsCertificateAlgorithm { get; set; }

        [JsonProperty("webRtcDtlsCertificateThumbnail")]
        public string WebRtcDtlsCertificateThumbnail { get; set; }

        [JsonProperty("webRtcIcePwd")]
        public string WebRtcIcePwd { get; set; }

        [JsonProperty("webRtcIceUfrag")]
        public string WebRtcIceUfrag { get; set; }
    }

    public class PartyChatLatencyRecord : IJsonSerializable
    {
        [JsonProperty("serverFqdn")]
        public string ServerFqdn { get; set; }

        [JsonProperty("targetLocation")]
        public string TargetLocation { get; set; }
    }

    public class PartyChatMemberRef : IJsonSerializable
    {
        [JsonProperty("gamertag")]
        public string GamerTag { get; set; }

        [JsonProperty("index")]
        public decimal Index { get; set; }

        [JsonProperty("partyId")]
        public Guid PartyId { get; set; }
    }

    public class PartyChatServer : IJsonSerializable
    {
        [JsonProperty("serverFqdn")]
        public string ServerFqdn { get; set; }

        [JsonProperty("targetLocation")]
        public string TargetLocation { get; set; }
    }

    public class PartyChatSession : IJsonSerializable
    {
        [JsonProperty("members")]
        public List<PartyChatSessionMember> Members { get; set; }

        [JsonProperty("partyId")]
        public Guid PartyId { get; set; }

        [JsonProperty("serverConnectionCandidiates")]
        public List<string> ServerConnectionCandidates { get; set; }
    }

    public class PartyChatSessionMember : IJsonSerializable
    {
        [JsonProperty("gamertag")]
        public string GamerTag { get; set; }

        [JsonProperty("mixerUser")]
        public List<PartyChatSessionMemberUser> MixerUser { get; set; }

        [JsonProperty("ref")]
        public PartyChatMemberRef Refferance { get; set; }

        [JsonProperty("simpleConnectionState")]
        public string SimpleConnectionState { get; set; }
    }

    public class PartyChatSessionMemberUser : IJsonSerializable
    {
        [JsonProperty("id")]
        public decimal Id { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }
    }

    public class PartyChatSubscription : IJsonSerializable
    {
        [JsonProperty("active")]
        public bool Acrive { get; set; }

        [JsonProperty("changeTypes")]
        public List<string> ChangeTypes { get; set; }

        [JsonProperty("connection")]
        public Guid Connection { get; set; }

        [JsonProperty("subscriptionId")]
        public Guid SubscriptionId { get; set; }
    }

    public class PlayerLayout : IJsonSerializable
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("order")]
        public List<decimal> Order { get; set; }

        [JsonProperty("players")]
        public PlayerLayoutPlayers Players { get; set; }
    }

    public class PlayerLayoutPlayers : IJsonSerializable
    {
        [JsonProperty("channelId")]
        public decimal channelId { get; set; }

        [JsonProperty("height")]
        public decimal Height { get; set; }

        [JsonProperty("interactivePinned")]
        public bool InteractivePinned { get; set; }

        [JsonProperty("left")]
        public decimal Left { get; set; }

        [JsonProperty("lowLatency")]
        public bool LowLatency { get; set; }

        [JsonProperty("top")]
        public decimal Top { get; set; }

        [JsonProperty("volume")]
        public decimal Volume { get; set; }

        [JsonProperty("width")]
        public decimal Width { get; set; }
    }

    public class PrivatePopulatedUser : PrivateUser
    {
        [JsonProperty("channel")]
        public Channel Channel { get; set; }

        [JsonProperty("prefferences")]
        public UserPreferences Preferences { get; set; }
    }

    public class PrivateUser : User
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("twoFactor")]
        public PrivateUserTfa TwoFactor { get; set; }
    }

    public class PrivateUserTfa : IJsonSerializable
    {
        [JsonProperty("codesViewed")]
        public bool CodesViewed { get; set; }

        [JsonProperty("enabled")]
        public bool Enabled { get; set; }
    }
}