﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Mixer.NET.Models
{
    public class Team : TimeStamped
    {
        [JsonProperty("backgroundUrl")]
        public string BackgroundUrl { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("logoUrl")]
        public string LogoUrl { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("ownerId")]
        public uint OwnerId { get; set; }

        [JsonProperty("social")]
        public SocialInfo Social { get; set; }

        [JsonProperty("token")]
        public string Token { get; set; }
    }

    public class TeamMembership : TimeStamped
    {
        [JsonProperty("accepted")]
        public bool Accepted { get; set; }

        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("teamId")]
        public uint TeamId { get; set; }

        [JsonProperty("userId")]
        public uint UserId { get; set; }
    }

    public class TeamMemebershipExpanded : TimeStamped
    {
        [JsonProperty("isPrimary")]
        public bool IsPrimary { get; set; }

        [JsonProperty("owner")]
        public User Owner { get; set; }

        [JsonProperty("teamMembership")]
        public TeamMembership TeamMembership { get; set; }
    }

    public class TestStreamSetting : IJsonSerializable
    {
        [JsonProperty("hoursLastReset")]
        public DateTime HoursLastReset { get; set; }

        [JsonProperty("hoursQuota")]
        public decimal HoursQuota { get; set; }

        [JsonProperty("hoursRemaning")]
        public decimal HoursRemaning { get; set; }

        [JsonProperty("hoursResetIntervalInDays")]
        public uint HoursResetIntervalInDays { get; set; }

        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("isActive")]
        public bool IsActive { get; set; }
    }

    public class TimeStamped : IJsonSerializable
    {
        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("deletedAt")]
        public DateTime DeletedAt { get; set; }

        [JsonProperty("updatedAt")]
        public DateTime UpdatedAt { get; set; }
    }

    public class TranscodingProfile : IJsonSerializable
    {
        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class TranscodingProfileTranscode : IJsonSerializable
    {
        [JsonProperty("bitrate")]
        public uint Bitrate { get; set; }

        [JsonProperty("fps")]
        public uint Fps { get; set; }

        [JsonProperty("height")]
        public uint Height { get; set; }

        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("requiresPartner")]
        public bool RequiresPartner { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("width")]
        public uint Width { get; set; }
    }

    public class TypeInformation : IJsonSerializable
    {
        [JsonProperty("aliases")]
        public List<string> Aliases { get; set; }

        [JsonProperty("availableAt")]
        public DateTime AvailableAt { get; set; }

        [JsonProperty("backgroundUrl")]
        public string BackgroundUrl { get; set; }

        [JsonProperty("coverUrl")]
        public string CoverUrl { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("online")]
        public decimal Online { get; set; }

        [JsonProperty("parent")]
        public string Parent { get; set; }

        [JsonProperty("source")]
        public string Source { get; set; }

        [JsonProperty("titles")]
        public List<TypeTitle> Titles { get; set; }

        [JsonProperty("verified")]
        public bool Verified { get; set; }

        [JsonProperty("viewersCurrent")]
        public decimal ViewersCurrent { get; set; }
    }

    public class TypeTitle : IJsonSerializable
    {
        [JsonProperty("aumId")]
        public string AumId { get; set; }

        [JsonProperty("kglId")]
        public string KglId { get; set; }

        [JsonProperty("processName")]
        public string ProcessName { get; set; }

        [JsonProperty("productId")]
        public string ProductId { get; set; }

        [JsonProperty("titleId")]
        public decimal? TitleId { get; set; }
    }
}