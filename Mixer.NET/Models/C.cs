﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Mixer.NET.Models
{
    public class Channel : TimeStamped
    {
        [JsonProperty("audiance")]
        public string Audiance { get; set; }

        [JsonProperty("badgeId")]
        public uint BadgeId { get; set; }

        [JsonProperty("bannerUrl")]
        public string BannerUrl { get; set; }

        [JsonProperty("costreamId")]
        public Guid CostreamId { get; set; }

        [JsonProperty("coverId")]
        public uint CoverId { get; set; }

        [JsonProperty("featured")]
        public bool Featured { get; set; }

        [JsonProperty("featureLevel")]
        public int FeatureLevel { get; set; }

        [JsonProperty("ftl")]
        public uint Ftl { get; set; }

        [JsonProperty("hasTranscodes")]
        public bool HasTranscodes { get; set; }

        [JsonProperty("hasVod")]
        public bool HasVod { get; set; }

        [JsonProperty("hosteeId")]
        public uint HosteeId { get; set; }

        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("interactive")]
        public bool Interactive { get; set; }

        [JsonProperty("interactiveGameId")]
        public uint InteractiveGameId { get; set; }

        [JsonProperty("languageId")]
        public string LanguageId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("numFollowers")]
        public uint NumFollowers { get; set; }

        [JsonProperty("online")]
        public bool Online { get; set; }

        [JsonProperty("partnered")]
        public bool Partnered { get; set; }

        [JsonProperty("preferences")]
        public ChannelPreferences Preferences { get; set; }

        [JsonProperty("suspended")]
        public bool Suspended { get; set; }

        [JsonProperty("thumbnailId")]
        public uint ThumbnailId { get; set; }

        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("transcodingProfileId")]
        public uint TranscodingProfileId { get; set; }

        [JsonProperty("typeId")]
        public uint TypeId { get; set; }

        [JsonProperty("userId")]
        public uint UserId { get; set; }

        [JsonProperty("viewersCurrent")]
        public uint ViewersCurrent { get; set; }

        [JsonProperty("viewersTotal")]
        public uint ViewersTotal { get; set; }

        [JsonProperty("vodsEnabled")]
        public bool VodsEnabled { get; set; }
    }

    public class ChannelAdvanced : Channel
    {
        [JsonProperty("type")]
        public GameType Type { get; set; }

        [JsonProperty("user")]
        public User User { get; set; }
    }

    public class ChannelAnalytic : IJsonSerializable
    {
        [JsonProperty("channel")]
        public uint Channel { get; set; }

        [JsonProperty("time")]
        public DateTime Time { get; set; }
    }

    public class ChannelPreferences : IJsonSerializable
    {
        [JsonProperty("channel:bannedwords")]
        public List<object> ChannelBannedwords { get; set; }

        [JsonProperty("channel:catbot:level")]
        public long ChannelCatbotLevel { get; set; }

        [JsonProperty("channel:chat:hostswitch")]
        public bool ChannelChatHostswitch { get; set; }

        [JsonProperty("channel:directPurchase:enabled")]
        public bool ChannelDirectPurchaseEnabled { get; set; }

        [JsonProperty("channel:donations:enabled")]
        public bool ChannelDonationsEnabled { get; set; }

        [JsonProperty("channel:links:allowed")]
        public bool ChannelLinksAllowed { get; set; }

        [JsonProperty("channel:links:clickable")]
        public bool ChannelLinksClickable { get; set; }

        [JsonProperty("channel:notify:directPurchase")]
        public bool ChannelNotifyDirectPurchase { get; set; }

        [JsonProperty("channel:notify:directPurchaseMessage")]
        public string ChannelNotifyDirectPurchaseMessage { get; set; }

        [JsonProperty("channel:notify:donate")]
        public bool ChannelNotifyDonate { get; set; }

        [JsonProperty("channel:notify:donatemessage")]
        public string ChannelNotifyDonatemessage { get; set; }

        [JsonProperty("channel:notify:follow")]
        public bool ChannelNotifyFollow { get; set; }

        [JsonProperty("channel:notify:followmessage")]
        public string ChannelNotifyFollowmessage { get; set; }

        [JsonProperty("channel:notify:hostedBy")]
        public string ChannelNotifyHostedBy { get; set; }

        [JsonProperty("channel:notify:hosting")]
        public string ChannelNotifyHosting { get; set; }

        [JsonProperty("channel:notify:subscribe")]
        public bool ChannelNotifySubscribe { get; set; }

        [JsonProperty("channel:notify:subscribemessage")]
        public string ChannelNotifySubscribemessage { get; set; }

        [JsonProperty("channel:offline:autoplayVod")]
        public bool ChannelOfflineAutoplayVod { get; set; }

        [JsonProperty("channel:partner:submail")]
        public string ChannelPartnerSubmail { get; set; }

        [JsonProperty("channel:player:muteOwn")]
        public bool ChannelPlayerMuteOwn { get; set; }

        [JsonProperty("channel:slowchat")]
        public long ChannelSlowchat { get; set; }

        [JsonProperty("channel:tweet:body")]
        public string ChannelTweetBody { get; set; }

        [JsonProperty("channel:tweet:enabled")]
        public bool ChannelTweetEnabled { get; set; }

        [JsonProperty("channel:users:levelRestrict")]
        public long ChannelUsersLevelRestrict { get; set; }

        [JsonProperty("costream:allow")]
        public string CostreamAllow { get; set; }

        [JsonProperty("hosting:allow")]
        public bool HostingAllow { get; set; }

        [JsonProperty("hosting:allowlive")]
        public bool HostingAllowlive { get; set; }

        [JsonProperty("hypezone:allow")]
        public bool HypezoneAllow { get; set; }

        [JsonProperty("mixer:featured:allow")]
        public bool MixerFeaturedAllow { get; set; }

        [JsonProperty("sharetext")]
        public string Sharetext { get; set; }
    }

    public class ChannelSuggestions : IJsonSerializable
    {
        [JsonProperty("actionsUrl")]
        public string ActionsUrl { get; set; }

        [JsonProperty("channels")]
        public List<Channel> Channels { get; set; }

        [JsonProperty("impressionUrl")]
        public string ImpressionUrl { get; set; }
    }

    public class ChatUser : IJsonSerializable
    {
        [JsonProperty("user")]
        public ChatUserLevel User { get; set; }

        [JsonProperty("userId")]
        public long UserId { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("userRoles"), JsonConverter(typeof(RoleConverter))]
        public Role UserRoles { get; set; }
    }

    public class ChatUserLevel : IJsonSerializable
    {
        [JsonProperty("experience")]
        public uint Experience { get; set; }

        [JsonProperty("level")]
        public uint Level { get; set; }
    }

    public class CheckoutItem : IJsonSerializable
    {
        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("quantity")]
        public uint Quantity { get; set; }
    }

    public class ClipProperties : IJsonSerializable
    {
        [JsonProperty("contentId")]
        public Guid ContentId { get; set; }

        [JsonProperty("contentLocators")]
        public List<Locator> ContentLocators { get; set; }

        [JsonProperty("contentMaturity")]
        public Maturity ContentMaturity { get; set; }

        [JsonProperty("durationInSeconds")]
        public uint DurationInSeconds { get; set; }

        [JsonProperty("expirationDate")]
        public DateTime ExpirationDate { get; set; }

        [JsonProperty("ownerChannelId")]
        public uint OwnerChannelId { get; set; }

        [JsonProperty("shareableId")]
        public string ShareableId { get; set; }

        [JsonProperty("streamerChannelId")]
        public uint StreamerChannelId { get; set; }

        [JsonProperty("tags")]
        public List<string> Tags { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("typeId")]
        public uint TypeId { get; set; }

        [JsonProperty("uploadDate")]
        public DateTime UploadDate { get; set; }

        [JsonProperty("viewCount")]
        public uint ViewCount { get; set; }
    }

    public class ClipRequest : IJsonSerializable
    {
        [JsonProperty("broadcastId")]
        public uint BroadcastId { get; set; }

        [JsonProperty("clipDurationInSeconds")]
        public uint ClipDurationInSeconds { get; set; }

        [JsonProperty("highlightTitle")]
        public string HighlightTitle { get; set; }
    }

    public class Confetti : IJsonSerializable
    {
        [JsonProperty("channelId")]
        public uint? ChannelId { get; set; }

        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("settings")]
        public ConfettiSettings Settings { get; set; }
    }

    public class ConfettiCircle : ConfettiParticleType
    {
        [JsonProperty("colors")]
        public ConfettiColor[] Colors { get; set; }

        [JsonProperty("flipPeriod")]
        public string FlipPeriod { get; set; }

        [JsonProperty("size")]
        public string Size { get; set; }
    }

    public class ConfettiColor : IJsonSerializable
    {
        [JsonProperty("b")]
        public string B { get; set; }

        [JsonProperty("g")]
        public string G { get; set; }

        [JsonProperty("probability")]
        public int Probability { get; set; }

        [JsonProperty("r")]
        public string R { get; set; }
    }

    public class ConfettiImage : ConfettiParticleType
    {
        [JsonProperty("data")]
        public string Data { get; set; }

        [JsonProperty("scale")]
        public string Scale { get; set; }

        [JsonProperty("spinPeriod")]
        public string SpinPeriod { get; set; }
    }

    public class ConfettiParticle : IJsonSerializable
    {
        [JsonProperty("draw")]
        public ConfettiParticleType Draw { get; set; }

        [JsonProperty("fader")]
        public string Fader { get; set; }

        [JsonProperty("lifetime")]
        public string Lifetime { get; set; }

        [JsonProperty("probability")]
        public int Probability { get; set; }

        [JsonProperty("velocity")]
        public string Velocity { get; set; }

        [JsonProperty("wiggleMagnitude")]
        public string WiggleMagnitude { get; set; }

        [JsonProperty("wigglePeriod")]
        public string WigglePeriod { get; set; }

        [JsonProperty("zdepth")]
        public string ZDepth { get; set; }
    }

    public class ConfettiParticleType : IJsonSerializable
    {
        [JsonProperty("shape")]
        public string Shape { get; set; }
    }

    public class ConfettiRectangle : ConfettiParticleType
    {
        [JsonProperty("colors")]
        public ConfettiColor[] Colors { get; set; }

        [JsonProperty("flipPeriod")]
        public string FlipPeriod { get; set; }

        [JsonProperty("height")]
        public string Height { get; set; }

        [JsonProperty("width")]
        public string Width { get; set; }
    }

    public class ConfettiSettings : IJsonSerializable
    {
        [JsonProperty("count")]
        public string Count { get; set; }

        [JsonProperty("particles")]
        public ConfettiParticle[] Particles { get; set; }
    }

    public class Costream : IJsonSerializable
    {
        [JsonProperty("capacity")]
        public uint Capacity { get; set; }

        [JsonProperty("channels")]
        public CostreamChannel[] Channels { get; set; }

        [JsonProperty("layout")]
        public PlayerLayout Layout { get; set; }

        [JsonProperty("startedAt")]
        public DateTime StartedAt { get; set; }
    }

    public class CostreamChannel : IJsonSerializable
    {
        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("userId")]
        public uint UserId { get; set; }
    }

    public class CPMAnalytic : ChannelAnalytic
    {
        [JsonProperty("impressions")]
        public uint Impressions { get; set; }

        [JsonProperty("payout")]
        public double Payout { get; set; }
    }
}