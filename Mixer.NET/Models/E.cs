﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Mixer.NET.Models
{
    public class EmojiRankAnalytic : ChannelAnalytic
    {
        [JsonProperty("count")]
        public uint Count { get; set; }

        [JsonProperty("emoji")]
        public string Emoji { get; set; }
    }

    public class EmoticonGroup : IJsonSerializable
    {
        [JsonProperty("emoji")]
        public string Emoji { get; set; }

        [JsonProperty("height")]
        public uint Height { get; set; }

        [JsonProperty("width")]
        public uint Width { get; set; }

        [JsonProperty("x")]
        public uint X { get; set; }

        [JsonProperty("y")]
        public uint Y { get; set; }
    }

    public class EmoticonPack : IJsonSerializable
    {
        [JsonProperty("channelId")]
        public uint ChannelId { get; set; }

        [JsonProperty("emoticons")]
        public List<EmoticonGroup> Emoticons { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }

    public class ExpandedChannel : ChannelAdvanced
    {
        [JsonProperty("badge")]
        public Resource Badge { get; set; }

        [JsonProperty("cache")]
        public object Cache { get; set; }

        [JsonProperty("cover")]
        public Resource Cover { get; set; }

        [JsonProperty("maxConcurrentSubscribers")]
        public uint MaxConcurrentSubscribers { get; set; }

        [JsonProperty("numSubscribers")]
        public uint NumSubscribers { get; set; }

        [JsonProperty("streamKey")]
        public string StreamKey { get; set; }

        [JsonProperty("thumbnail")]
        public Resource Thumbnail { get; set; }

        [JsonProperty("totalUniqueSubscribers")]
        public uint TotalUniqueSubscribers { get; set; }
    }

    public class ExpandedRedeemable : Redeemable
    {
        [JsonProperty("owner")]
        public User Owner { get; set; }

        [JsonProperty("redeemedBy")]
        public User RedeemedBy { get; set; }
    }

    public class ExpandedShare : Share
    {
        [JsonProperty("user")]
        public User User { get; set; }
    }

    public class ExpandedTranscodingProfile : TranscodingProfile
    {
        [JsonProperty("transcodes")]
        public TranscodingProfileTranscode[] Transcodes { get; set; }
    }
}