﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Mixer.NET.Models
{
    public class Language : IJsonSerializable
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class LanguageCount : IJsonSerializable
    {
        [JsonProperty("count")]
        public uint Count { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }

    public class LightVideoManifest : IJsonSerializable
    {
        [JsonProperty("resolutions")]
        public List<VideoManifestResolution> Resolutions { get; set; }

        [JsonProperty("since")]
        public DateTime Since { get; set; }
    }

    public class Locator : IJsonSerializable
    {
        [JsonProperty("locatorType")]
        public string LocatorType { get; set; }

        [JsonProperty("uri")]
        public string Uri { get; set; }
    }

    public class Light2Manifest : IJsonSerializable
    {
        [JsonProperty("hlsSrc")]
        public string HlsSrc { get; set; }
        
        [JsonProperty("ftlSrc")]
        public string FtlSrc { get; set; }

        [JsonProperty("startedAt")]
        public DateTime StartedAt { get; set; }

        [JsonProperty("now")]
        public DateTime Now { get; set; }

        [JsonProperty("isTestStream")]
        public bool TestStream { get; set; }
        
        [JsonProperty("accessKey")]
        public string AccessKey { get; set; }

    }
}