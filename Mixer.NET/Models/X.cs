﻿using Newtonsoft.Json;

namespace Mixer.NET.Models
{
    public class XblFriendsResponse : IJsonSerializable
    {
        [JsonProperty("avatarUrl")]
        public string AvatarUrl { get; set; }

        [JsonProperty("channelId")]
        public uint ChannelId { get; set; }

        [JsonProperty("level")]
        public uint Level { get; set; }

        [JsonProperty("online")]
        public bool Online { get; set; }

        [JsonProperty("userId")]
        public uint UserId { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }
    }
}