﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Mixer.NET.Models
{
    public class Notification : IJsonSerializable
    {
        [JsonProperty("payload")]
        public string Payload { get; set; }

        [JsonProperty("sentAt")]
        public DateTime SentAt { get; set; }

        [JsonProperty("trigger")]
        public string Trigger { get; set; }

        [JsonProperty("userId")]
        public uint UserID { get; set; }
    }

    public class NotificationPreference : IJsonSerializable
    {
        [JsonProperty("allowsEmailMarketing")]
        public string AllowsEmailMarketing { get; set; }

        [JsonProperty("health")]
        public dynamic Health { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("lastRead")]
        public DateTime LastRead { get; set; }

        [JsonProperty("liveNotifications")]
        public List<object> LiveNotifications { get; set; }

        [JsonProperty("liveOnByDefault")]
        public List<string> LiveOnByDefault { get; set; }

        [JsonProperty("notifyFollower")]
        public List<string> NotifyFollower { get; set; }

        [JsonProperty("notifySubscriber")]
        public List<string> NotifySubscriber { get; set; }

        [JsonProperty("transports")]
        public List<object> Transports { get; set; }
    }

    public class NotificationPrefrenceHealth : IJsonSerializable
    {
        [JsonProperty("eat")]
        public decimal Eat { get; set; }

        [JsonProperty("stretch")]
        public decimal Stretch { get; set; }
    }
}