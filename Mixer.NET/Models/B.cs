﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mixer.NET.Models
{
    public class Broadcast : IJsonSerializable
    {
        [JsonProperty("channelId")]
        public uint ChannelId { get; set; }

        [JsonProperty("id")]
        public Guid Id { get; set; }
        [JsonProperty("online")]
        public bool Online { get; set; }

        [JsonProperty("startedAt")]
        public DateTime StartedAt { get; set; }

        [JsonProperty("isTestStream")]
        public bool TestStream { get; set; }
    }
}
