﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Mixer.NET.Models
{
    public class Ingest : IJsonSerializable
    {
        [JsonProperty("host")]
        public string Host { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("pingTest")]
        public string PingTest { get; set; }

        [JsonProperty("protocols"), JsonConverter(typeof(TypeArrayConverter))]
        public List<string> Protocols { get; set; }
    }

    public class IngestStreamAnalytic : IJsonSerializable
    {
        [JsonProperty("audio_avg_nack_delay")]
        public uint AudioAvgNackDelay { get; set; }

        [JsonProperty("audioo_jitter_buffer_size")]
        public uint AudiooJitterBufferSize { get; set; }

        [JsonProperty("audiov_avg_recovery_delay")]
        public uint AudiovAvgRecoveryDelay { get; set; }

        [JsonProperty("audiov_bitrate")]
        public uint AudiovBitrate { get; set; }

        [JsonProperty("audiovo_packet_loss")]
        public int AudiovoPacketLoss { get; set; }

        [JsonProperty("audiov_packet_recovered")]
        public int AudiovPacketRecovered { get; set; }

        [JsonProperty("channel_id")]
        public uint ChannelId { get; set; }

        [JsonProperty("ingest_id")]
        public uint IngestId { get; set; }

        [JsonProperty("subscribed_dist_nodes")]
        public uint SubscribedDistNodes { get; set; }

        [JsonProperty("video_avg_nack_delay")]
        public uint VideoAvgNackDelay { get; set; }

        [JsonProperty("video_avg_recovery_delay")]
        public uint VideoAvgRecoveryDelay { get; set; }

        [JsonProperty("video_bitrate")]
        public uint VideoBitrate { get; set; }

        [JsonProperty("video_jitter_buffer_size")]
        public uint VideoJitterBufferSize { get; set; }

        [JsonProperty("video_packet_loss")]
        public int VideoPacketLoss { get; set; }

        [JsonProperty("video_packet_recovered")]
        public int VideoPacketRecovered { get; set; }
    }

    public class InteractiveAnalysisCoords : IJsonSerializable
    {
        [JsonProperty("mean")]
        public bool Mean { get; set; }

        [JsonProperty("stdDev")]
        public bool StdDev { get; set; }
    }

    public class InteractiveAnalytic : ChannelAnalytic
    {
        [JsonProperty("inputsGiven")]
        public uint InputsGiven { get; set; }

        [JsonProperty("playtime")]
        public uint Playtime { get; set; }

        [JsonProperty("sparksSpent")]
        public Dictionary<string, int> SparksSpent { get; set; }

        [JsonProperty("streamLength")]
        public uint StreamLength { get; set; }

        [JsonProperty("streams")]
        public uint Streams { get; set; }

        [JsonProperty("timeToInteractive")]
        public Dictionary<string, int> TimeToInteractive { get; set; }

        [JsonProperty("viewers")]
        public uint Viewers { get; set; }
    }

    public class InteractiveBlueprintBase : IJsonSerializable
    {
        [JsonProperty("state")]
        public string State { get; set; }
    }

    public class InteractiveBlueprintExtended : InteractiveBlueprintBase
    {
        [JsonProperty("grid")]
        public string Grid { get; set; }

        [JsonProperty("height")]
        public uint Height { get; set; }

        [JsonProperty("width")]
        public uint Width { get; set; }

        [JsonProperty("x")]
        public uint X { get; set; }

        [JsonProperty("y")]
        public uint Y { get; set; }
    }

    public class InteractiveConnectionInfo : IJsonSerializable
    {
        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("influence")]
        public int Influence { get; set; }

        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("version")]
        public InteractiveVersion Version { get; set; }
    }

    public class InteractiveControls : IJsonSerializable
    {
        [JsonProperty("joysticks")]
        public List<InteractiveControlsJoyStick> Joysticks { get; set; }

        [JsonProperty("reportInterval")]
        public uint ReportInterval { get; set; }

        [JsonProperty("screens")]
        public List<InteractiveControlsScreen> Screens { get; set; }

        [JsonProperty("tactiles")]
        public List<InteractiveControlsTactile> Tactiles { get; set; }
    }

    public class InteractiveControlsBase : IJsonSerializable
    {
        [JsonProperty("id")]
        public uint Id { get; set; }
    }

    public class InteractiveControlsJoyStick : InteractiveControlsBase
    {
        [JsonProperty("analysis")]
        public InteractiveJoyStickAnalysis Analysis { get; set; }

        [JsonProperty("blueprint")]
        public List<InteractiveBlueprintExtended> Blueprint { get; set; }
    }

    public class InteractiveControlsScreen : InteractiveControlsBase
    {
        [JsonProperty("analysis")]
        public List<InteractiveScreenAnalysis> Analysis { get; set; }

        [JsonProperty("blueprint")]
        public List<InteractiveBlueprintBase> Blueprint { get; set; }
    }

    public class InteractiveControlsTactile : InteractiveControlsBase
    {
#pragma warning disable CS0649
        [JsonExtensionData]
        private IDictionary<string, JToken> additionalData;
#pragma warning restore CS0649

        [JsonProperty("analysis")]
        public InteractiveTactileAnalysis Analysis { get; set; }

        [JsonProperty("blueprint")]
        public List<InteractiveBlueprintExtended> Blueprint { get; set; }

        public uint Cooldown { get; set; }

        public uint Cost { get; set; }

        [JsonProperty("key")]
        public uint Key { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }

        [OnDeserialized]
        public void OnDeserialized(StreamingContext context)
        {
            Cost = ((IDictionary<string, JToken>)((IDictionary<string, JToken>)additionalData["cost"])["press"])["cost"].ToUInt();
            Cooldown = ((IDictionary<string, JToken>)additionalData["cooldown"])["press"].ToUInt();
        }
    }

    public class InteractiveGame : TimeStamped
    {
        [JsonProperty("coverUrl")]
        public string CoverUrl { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("hasPublishedVersions")]
        public string HasPublishedVersions { get; set; }

        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("installation")]
        public string installation { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("ownerId")]
        public uint OwnerId { get; set; }

        [JsonProperty("typeId")]
        public uint TypeId { get; set; }
    }

    public class InteractiveGameListing : InteractiveGame
    {
        [JsonProperty("owner")]
        public uint Owner { get; set; }

        [JsonProperty("versions")]
        public List<InteractiveGameVersions> Versions { get; set; }
    }

    public class InteractiveGameVersions
    {
        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("versionOrder")]
        public uint VersionOrder { get; set; }

        [JsonProperty("versions")]
        public string Versions { get; set; }
    }

    public class InteractiveJoyStickAnalysis : IJsonSerializable
    {
        [JsonProperty("coords")]
        public InteractiveAnalysisCoords Coords { get; set; }
    }

    public class InteractiveMetric : IJsonSerializable
    {
        [JsonProperty("bundle")]
        public string Bundle { get; set; }

        [JsonProperty("channelId")]
        public string ChannelId { get; set; }

        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("mixerTypeId")]
        public string MixerTypeId { get; set; }

        [JsonProperty("projectId")]
        public string ProjectId { get; set; }

        [JsonProperty("published")]
        public bool Published { get; set; }

        [JsonProperty("publisherUserId")]
        public string PublisherUserId { get; set; }

        [JsonProperty("sessionLenght")]
        public uint SessionLenght { get; set; }

        [JsonProperty("timestamp")]
        public DateTime Timestamp { get; set; }

        [JsonProperty("totalPlaytimeHours")]
        public uint TotalPlaytimeHours { get; set; }

        [JsonProperty("versionId")]
        public string VersionId { get; set; }
    }

    public class InteractiveScreenAnalysis : IJsonSerializable
    {
        [JsonProperty("clicks")]
        public bool Clicks { get; set; }

        [JsonProperty("position")]
        public InteractiveAnalysisCoords Position { get; set; }
    }

    public class InteractiveTactileAnalysis
    {
        [JsonProperty("frequency")]
        public bool Frequency { get; set; }

        [JsonProperty("holding")]
        public bool Holding { get; set; }
    }

    public class InteractiveVersion : TimeStamped
    {
        [JsonProperty("bundle")]
        public string Bundle { get; set; }

        [JsonProperty("changelog")]
        public string Changelog { get; set; }

        [JsonProperty("controls")]
        public InteractiveControls Controls { get; set; }

        [JsonProperty("controlVersion")]
        public string ControlVersion { get; set; }

        [JsonProperty("download")]
        public string Download { get; set; }

        [JsonProperty("gameId")]
        public uint GameId { get; set; }

        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("installation")]
        public string Installation { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }

        [JsonProperty("versionOrder")]
        public uint VersionOrder { get; set; }
    }

    public class InteractiveViewerAnalytic : ChannelAnalytic
    {
        [JsonProperty("browser")]
        public string Browser { get; set; }

        [JsonProperty("channelId")]
        public double ChannelId { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("platform")]
        public string Platform { get; set; }

        [JsonProperty("referrer")]
        public string Referrer { get; set; }
    }

    public class Invoice
    {
        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("items")]
        public List<InvoiceItem> Items { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("user")]
        public uint User { get; set; }
    }

    public class InvoiceItem : TimeStamped
    {
        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("invoice")]
        public uint Invoice { get; set; }

        [JsonProperty("quantity")]
        public uint Quantity { get; set; }

        [JsonProperty("relid")]
        public uint? RelId { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("user")]
        public uint User { get; set; }
    }
}