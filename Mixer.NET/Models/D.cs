﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Mixer.NET.Models
{
    public enum DiscordType
    {
        Voice,
        Text,
        Category
    }

    public class DelveBackground : IJsonSerializable
    {
        [JsonProperty("fullWidthSrc")]
        public string FullWidthSrc { get; set; }

        [JsonProperty("leftSrc")]
        public string LeftSrc { get; set; }

        [JsonProperty("rightSrc")]
        public string RigthSrc { get; set; }
    }

    public class DelveCarousel : DelveType
    {
        [JsonProperty("channels")]
        public DelveCarouselChannel[] Channels { get; set; }
    }

    public class DelveCarouselChannel : IJsonSerializable
    {
        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("type")]
        public GameType Type { get; set; }

        [JsonProperty("userId")]
        public uint UserId { get; set; }

        [JsonProperty("viewersCurrent")]
        public uint ViewersCurrent { get; set; }
    }

    public class DelveChannel : DelveType
    {
        [JsonProperty("hydration")]
        public DelveChannelInner Hydration { get; set; }

        [JsonProperty("morePath")]
        public string MorePath { get; set; }

        [JsonProperty("style")]
        public string Style { get; set; }

        [JsonProperty("title")]
        public object Title { get; set; }
    }

    public class DelveChannelGameType : IJsonSerializable
    {
        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("interactive")]
        public bool Interactive { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("online")]
        public bool Online { get; set; }

        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("type")]
        public GameType Type { get; set; }

        [JsonProperty("userId")]
        public uint UserId { get; set; }

        [JsonProperty("viewerCurrent")]
        public uint ViewerCurrent { get; set; }
    }

    public class DelveChannelInner : IJsonSerializable
    {
        [JsonProperty("apiPath")]
        public string ApiPath { get; set; }

        [JsonProperty("continuation")]
        public string Continuation { get; set; }

        [JsonProperty("results")]
        public DelveChannelGameType Results { get; set; }
    }

    public class DelveError : DelveType
    {
        [JsonProperty("originalType")]
        public string OriginalType { get; set; }
    }

    public class DelveGames : DelveType
    {
        [JsonProperty("hydration")]
        public DelveGamesInner Hydration { get; set; }

        [JsonProperty("morePath")]
        public string MorePath { get; set; }

        [JsonProperty("title")]
        public object Title { get; set; }
    }

    public class DelveGamesGameType : IJsonSerializable
    {
        [JsonProperty("coverUrl")]
        public string CoverUrl { get; set; }

        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("viewerCurrent")]
        public uint ViewerCurrent { get; set; }
    }

    public class DelveGamesInner : IJsonSerializable
    {
        [JsonProperty("apiPath")]
        public string ApiPath { get; set; }

        [JsonProperty("continuation")]
        public string Continuation { get; set; }

        [JsonProperty("results")]
        public DelveGamesGameType Results { get; set; }
    }

    public class DelveMixPlayFilters : IJsonSerializable
    {
        [JsonProperty("description")]
        public object Description { get; set; }

        [JsonProperty("integrationIds")]
        public List<uint> IntegrationIds { get; set; }

        [JsonProperty("name")]
        public object Name { get; set; }
    }

    public class DelveOnlyOnMixer : IJsonSerializable
    {
        [JsonProperty("channel")]
        public object Channel { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("typeId")]
        public int TypeId { get; set; }
    }

    public class DelveType : IJsonSerializable
    {
        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public class DiscordBot : IJsonSerializable
    {
        [JsonProperty("channelId")]
        public uint ChannelId { get; set; }

        [JsonProperty("guildId")]
        public string GuildId { get; set; }

        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("inviteChannel")]
        public string InviteChannel { get; set; }

        [JsonProperty("inviteSetting")]
        public string InviteSetting { get; set; }

        [JsonProperty("liveAnnounceChannel")]
        public string LiveAnnounceChannel { get; set; }

        [JsonProperty("liveChatChannel")]
        public string LiveChatChannel { get; set; }

        [JsonProperty("liveUpdateState")]
        public bool LiveUpdateState { get; set; }

        [JsonProperty("roles")]
        public DiscordBotInnerRole Roles { get; set; }

        [JsonProperty("syncEmoteRoles")]
        public List<uint> SyncEmoteRoles { get; set; }
    }

    public class DiscordBotInnerRole : IJsonSerializable
    {
        [JsonProperty("gracePeriod")]
        public uint GracePeriod { get; set; }

        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("roleId")]
        public string RoleId { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public class DiscordChannel : IJsonSerializable
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("private")]
        public bool Private { get; set; }

        [JsonProperty("type")]
        public DiscordType Type { get; set; }
    }

    public class DiscordRole : IJsonSerializable
    {
        [JsonProperty("assignable")]
        public bool Assignable { get; set; }

        [JsonProperty("color")]
        public string Color { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}