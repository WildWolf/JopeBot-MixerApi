﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Mixer.NET.Models
{
    [Flags]
    public enum Role
    {
        Owner = 1 << 10,
        Founder = 1 << 9,
        Staff = 1 << 8,
        ChannelEditor = 1 << 7,
        GlobalMod = 1 << 6,
        Mod = 1 << 5,
        VerifiedPartner = 1 << 4,
        Partner = 1 << 3,
        Subscriber = 1 << 2,
        Pro = 1 << 1,
        User = 1 << 0,
        Banned = 0
    }

    public class Recording : TimeStamped
    {
        [JsonProperty("channelId")]
        public uint ChannelId { get; set; }

        [JsonProperty("duration")]
        public decimal Duration { get; set; }

        [JsonProperty("expiresAt")]
        public DateTime ExpiresAt { get; set; }

        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("seen")]
        public bool Seen { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("typeId")]
        public uint TypeId { get; set; }

        [JsonProperty("viewed")]
        public bool Viewed { get; set; }

        [JsonProperty("viewsTotal")]
        public uint ViewsTotal { get; set; }

        [JsonProperty("vods")]
        public List<VOD> Vods { get; set; }
    }

    public class RecurringPayment : TimeStamped
    {
        [JsonProperty("cancelled")]
        public bool Cancelled { get; set; }

        [JsonProperty("gateway")]
        public string Gateway { get; set; }

        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("relid")]
        public uint RelId { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("timesPaid")]
        public uint TimesPaid { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("user")]
        public uint User { get; set; }
    }

    public class RecurringPaymentExtended : RecurringPayment
    {
        [JsonProperty("subscription")]
        public SubscriptionExtended Subscription { get; set; }
    }

    public class Redeemable : TimeStamped
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("meta")]
        public object Meta { get; set; }

        [JsonProperty("ownerId")]
        public uint OwnerId { get; set; }

        [JsonProperty("redeemedAt")]
        public DateTime RedeemedAt { get; set; }

        [JsonProperty("redeemedById")]
        public uint RedeemedById { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public class RedeemablePartner : TimeStamped
    {
        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("partnerId")]
        public uint PartnerId { get; set; }

        [JsonProperty("redeemableId")]
        public uint RedeemableId { get; set; }
    }

    public class Resource : IJsonSerializable
    {
        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("meta")]
        public object Meta { get; set; }

        [JsonProperty("relid")]
        public uint RelId { get; set; }

        [JsonProperty("remotePath")]
        public string RemotePath { get; set; }

        [JsonProperty("store")]
        public string Store { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }
}