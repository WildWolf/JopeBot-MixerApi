﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Mixer.NET.Models
{
    public enum WebhookDeactivationCode
    {
        Manual = 0,
        Event = 1,
        Failed = 2
    }

    public class Webhook : IJsonSerializable
    {
        [JsonProperty("deactivationReasons")]
        public WebhookDeactivation DeactivationReasons { get; set; }

        [JsonProperty("events")]
        public List<string> Events { get; set; }

        [JsonProperty("expiresAt")]
        public DateTime ExpiresAt { get; set; }

        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("isActive")]
        public bool IsActive { get; set; }

        [JsonProperty("kind")]
        public string Kind { get; set; }

        [JsonProperty("renewUrl")]
        public string RenewUrl { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }

    public class WebhookDeactivation : IJsonSerializable
    {
        [JsonProperty("code")]
        public WebhookDeactivationCode Code { get; set; }

        [JsonProperty("reason")]
        public string Reason { get; set; }
    }
}