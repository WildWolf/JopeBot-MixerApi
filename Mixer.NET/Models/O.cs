﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Mixer.NET.Models
{
    public class OAuthAuthorization : IJsonSerializable
    {
        [JsonProperty("client")]
        public OAuthClient Client { get; set; }

        [JsonProperty("premissions")]
        public List<string> Permissions { get; set; }

        [JsonProperty("userId")]
        public uint UserId { get; set; }
    }

    public class OAuthClient : IJsonSerializable
    {
        [JsonProperty("clientId")]
        public string ClientId { get; set; }

        [JsonProperty("hosts")]
        public List<string> Hosts { get; set; }

        [JsonProperty("id")]
        public uint Id { get; set; }

        [JsonProperty("logo")]
        public string Logo { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("website")]
        public string Website { get; set; }
    }

    public class OAuthLink : IJsonSerializable
    {
        [JsonProperty("service")]
        public string Service { get; set; }

        [JsonProperty("serviceId")]
        public string ServiceId { get; set; }

        [JsonProperty("userId")]
        public decimal UserId { get; set; }
    }

    public class OAuthShortCodeResponse : IJsonSerializable
    {
        [JsonProperty("code")]
        public string Code { get; set; }
        
        [JsonProperty("handle")]
        public string Handle { get; set; }

        [JsonProperty("expires_in")]
        public uint Expiration { get; set; }

    }
}